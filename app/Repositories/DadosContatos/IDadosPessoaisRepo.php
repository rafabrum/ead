<?php

namespace App\Repositories\DadosContatos;

interface IDadosPessoaisRepo
{
	public function criarDadosPessoais($request);

	public function alterarDadosPessoais($request);
}
