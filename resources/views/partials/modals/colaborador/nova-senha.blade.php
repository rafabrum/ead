<div class="modal inmodal fade alterar-menu" id="modalNovaSenha{{$colaborador->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Nova Senha - {{$colaborador->user->name}} {{$colaborador->user->sobrenome}}</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postAlterarSenhaColaborador') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<input type="hidden" name="colaborador_id" value="{{ $colaborador->id }}" />
								<div class="col-md-6">
									<div class="form-group">
										<label>Senha</label>

										<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

										@if ($errors->has('password'))
										<span class="help-block">
											<p>{{ $errors->first('password') }}</p>
										</span>
										@endif
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="password-confirm" class="col-md-6 col-form-label text-md-right">Confirme a senha</label>

										<input type="password" class="form-control" name="password_confirmation" required>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" >Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
