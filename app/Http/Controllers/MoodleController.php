<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoriaMenu;
use App\Models\Menu;
use App\Models\Colaborador;
use App\Repositories\Moodle\IMoodleRepo;
use Illuminate\Support\Facades\Crypt;
use DB;
use Auth;

class MoodleController extends Controller
{
    public function acessoMoodle(){

    	$usuario = Auth::user()->colaborador->usuario_moodle;
    	$senha = base64_decode(Auth::user()->colaborador->senha_moodle);

    	return view('moodle.acesso-moodle')
    		   ->with('senha', $senha)
    		   ->with('usuario', $usuario);
    }
}
