<div class="modal inmodal fade alterar-menu" id="modalNovaCategoria" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Cadastro de Categoria</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postNovaCategoria') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<div class="col-md-4">
									<div class="form-group">
										<label>Nome</label>
										<input type="text" name="nome" class="form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Ordem</label>
										<input type="text" name="ordem" class="form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Icone</label>
										<input type="text" name="icone" class="form-control">
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" id="btnSalvarCampanha">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
