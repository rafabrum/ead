<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaMenu extends Model
{
    protected $table = 'categoria_menu';
    protected $primaryKey = 'id';

    protected $fillable = ["nome", "ordem", "icone"];

    public function menu()
    {
        return $this->hasOne('App\Models\Menu', "categoria_id", "id");
    }
}
