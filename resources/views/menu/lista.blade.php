@extends('layouts.app')
@section('styles-plugins')
<link href="{{ elixir('assets/template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
#categoriaMenu{
    background-color: #82c2ed;
    padding: 5px;
    width: 30px;
    border-radius: 3px;
    margin-right: 10px;
    color: white;
}
#editMenu{
    padding: 5px;
    border-radius: 3px;
    margin-right: 5px;
}
#novaAcao{
    margin-top: 10px;
}
.sub-menu{
    margin-left: 40px;
    margin-right: 40px;
}
</style>
@endsection
@section('scripts-plugins')
<script src="{{ elixir('assets/template/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Menus</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="panel-group">
                        @foreach(App\Http\Controllers\Controller::CategoriasMenu() as $categoria)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse{{$categoria->id}}"><i id="categoriaMenu" class="{{$categoria->icone}}"></i> {{$categoria->nome}}</a>
                                    <a data-toggle="modal" data-target="#modalAlterarCategoria{{$categoria->id}}" href="" title="Alterar"><i class="fa fa-file-text-o text-navy pull-right"></i></a>
                                </h4>
                            </div>
                            <div id="collapse{{$categoria->id}}" class="panel-collapse collapse">
                                @foreach($menus as $menu)
                                @if($menu->categoria_id == $categoria->id)
                                <div class="panel-body sub-menu">
                                    <h4 class="panel-title">
                                    <a data-toggle="modal" data-target="#modalAlterarMenu{{$menu->id, $menu->categoria->id}}" href="" title="Alterar"><i id="editMenu" class="fa fa-pencil"></i>{{$menu->titulo}}</a>
                                    </h4>
                                </div>
                                @include('partials.modals.menu.alterar', ['menu' => $menu])
                                @endif
                                @endforeach
                                <div class="panel-body sub-menu">
                                    <h4 class="panel-title">
                                        <a data-toggle="modal" data-target="#modalNovoMenu{{$categoria->id}}" href="" title="Novo menu"><i id="editMenu" class="fa fa-plus"></i> Novo Menu</a>
                                    </h4>
                                </div>
                            </div>
                    </div>
                    @include('partials.modals.menu.cadastrar', ['categoria' => $categoria])
                    @include('partials.modals.menu.alterar-categoria', ['categoria' => $categoria])
                    @endforeach
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="modal" data-target="#modalNovaCategoria" href="" title="Nova Categoria"><i id="categoriaMenu" class="fa fa-plus"></i> Nova Categoria</a>
                        </h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('partials.modals.menu.cadastrar-categoria')
@endsection
