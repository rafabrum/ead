<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco', function (Blueprint $table) {
            $table->increments('id');
            $table->string('end_rua', 60)->nullable();
            $table->integer('end_numero')->nullable();
            $table->string('end_complemento', 15)->nullable();
            $table->string('end_bairro', 50)->nullable();
            $table->string('end_cidade', 50)->nullable();
            $table->string('end_cep', 15)->nullable();
            $table->string('end_estado', 50)->nullable();
            $table->timestamps();

            $table->unsignedinteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endereco');
    }
}
