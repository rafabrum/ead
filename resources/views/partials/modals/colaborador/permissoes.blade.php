<div class="modal inmodal fade alterar-menu" id="modalPermissoes{{$colaborador->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Permissões {{$colaborador->user->name}} {{$colaborador->user->sobrenome}}</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postAlterarPermissoesColaborador') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{$colaborador->id}}" />
								@foreach($menus as $key => $menu)
								<div class="col-md-6">
									<label>{{$menu->titulo}}</label>
									@if($colaborador->menus->contains($menu->id))
									<input type="checkbox" checked name="{{$key}}" class="pull-right" value="{{$menu->id}}">
									@else
									<input type="checkbox" name="{{$key}}" class="pull-right" value="{{$menu->id}}">
									@endif
									<legend></legend>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
