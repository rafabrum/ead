<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColaboradorMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaborador_menus', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedinteger('colaborador_id');

            $table->foreign('colaborador_id')->references('id')->on('colaboradores');

            $table->unsignedinteger('menu_id');

            $table->foreign('menu_id')->references('id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colaborador_menus');
    }
}
