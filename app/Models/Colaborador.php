<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colaborador extends Model
{

	protected $table = 'colaboradores';
    protected $primaryKey = 'id';

    protected $fillable = ["ramal", "status",
							"formacao", "moodle_id", "user_id", "usuario_moodle", "senha_moodle", "tipo_usuario_moodle", "id_tipo_usuario_moodle"];

	public function user()
    {
         return $this->belongsTo('App\User', "user_id", "id");
    }

    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu','colaborador_menus', 'colaborador_id','menu_id');
                    
    }
}
