<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $data = "2018-08-21 03:00:00";

    public function run()
    {

     DB::table('menus')->insert([
        'id' => 1,
        'titulo' => 'Lista Aluno',
        'alias' => 'lista_aluno',
        'url' => 'listaAluno',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 1
    ]);

     DB::table('menus')->insert([
        'id' => 2,
        'titulo' => 'Menus',
        'alias' => 'lista_menu',
        'url' => 'listaMenu',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 5
    ]);

     DB::table('menus')->insert([
        'id' => 3,
        'titulo' => 'Acesso Moodle',
        'alias' => 'acesso_moodle',
        'url' => 'acessoMoodle',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 2
    ]);

     DB::table('menus')->insert([
        'id' => 4,
        'titulo' => 'Alunos',
        'alias' => 'lista_aluno',
        'url' => 'listaAluno',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 1
    ]);

     DB::table('menus')->insert([
        'id' => 5,
        'titulo' => 'Cursos',
        'alias' => 'lista_curso',
        'url' => 'listaCurso',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 4
    ]);

    DB::table('menus')->insert([
        'id' => 6,
        'titulo' => 'Colaboradores',
        'alias' => 'lista_colaborador',
        'url' => 'listaColaborador',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 2
    ]);

        DB::table('menus')->insert([
        'id' => 7,
        'titulo' => 'Novo colaborador',
        'alias' => 'novo_colaborador',
        'url' => 'viewNovoColaborador',
        'created_at' => $this->data,
        'updated_at' => $this->data,
        'categoria_id' => 2
    ]);
 }
}