<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Colaborador;
use App\User;
use App\Models\Menu;
use App\Models\ColaboradorMenus;
use App\Repositories\Colaborador\IColaboradorRepo;
use App\Repositories\Usuario\IUsuarioRepo;
use DB;
use Session;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Moodle\IMoodleRepo;


class ColaboradorController extends Controller
{

    public function viewNovoColaborador(){

        return view('colaborador.novo');
    }

    public function viewListaColaborador(){

    	$colaboradores = Colaborador::all();
    	$menus = Menu::all();

        return view('colaborador.lista')
                ->with('menus', $menus)
                ->with('colaboradores', $colaboradores);

    }

    public function alterarPermissoesColaborador(Request $request){

        $menus = Menu::all()->count();
        $i = 0;

       //deleta as relações de colaborador e menu para recadastrar  
       DB::table('colaborador_menus')->where('colaborador_id', $request->id)->delete();

        while ($i <= $menus) {
            if($request->input($i)){
                DB::table('colaborador_menus')->insert(['colaborador_id' => $request->id, 'menu_id' => $request->$i]);
            }
            $i++;
        }

        return redirect(url()->previous());
    }

    public function alterarColaborador(Request $request, IMoodleRepo $moodle, IColaboradorRepo $colaborador, IUsuarioRepo $usuario){

        //salvar o id do colaborador para identificar qual modal será reaberta
        Session::flash('modal', $request->colaborador_id);

        $this->validarDados($request);

        $dados = db::table('colaboradores')->where('id', $request->colaborador_id)->get();

        $usuario->alterarUsuario($request);
        $removerTipoUsuarioMoodle = $moodle->removerTipoUsuarioMoodle($dados[0]->id_tipo_usuario_moodle, $dados[0]->moodle_id);
        $tipoUsuarioMoodle = $moodle->definirTipoUsuarioMoodle($request, $dados[0]->moodle_id);
        $dadosColaborador =  $colaborador->tratarTipoUsuarioMoodle($request);
        $colaborador->alterarColaborador($dadosColaborador);

        return $this->tratarRespostaComAlertaNotificacao( "OK!", "Colaborador salvo com sucesso!", route('listaColaborador'));
    }


    public function alterarSenhaColaborador(Request $request){

       Session::flash('modalSenha', $request->colaborador_id);
       $this->validarSenha($request);

       $colaborador = Colaborador::find($request->colaborador_id);

       $alterarUsuario = User::where('id', $colaborador->user_id)
              ->update([
                  'password' => Hash::make($request->password),
               ]);

        return redirect(url()->previous());
    }

    
    protected function validarDados($request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:40',
            'sobrenome' => 'required|string|min:3|max:40',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->usuario_id,
        ], [
          'required' => 'Campo obrigatório',
          'email.unique' => 'Já existe um usuário com esse email',
          'min' => 'Minimo de 3 caracteres',
          'max' => 'maximo de 40 caracteres',
        ]);
    }

        protected function validarSenha($request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9_])/|confirmed',
        ], [
          'password.min' => 'Mínino de 8 caracteres',
          'password.confirmed' => 'As senhas digitadas não conferem',
          'regex' => 'A senha deve conter uma letra maiuscula um numero e um caracter especial'
        ]);
    }
}
