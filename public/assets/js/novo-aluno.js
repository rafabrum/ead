
$(document).ready(function(){

	$("#listaColaborador").css("display", "none");
	$("#listaAgenciador").css("display", "none");

	$("#wizard").steps({
		labels: {
			cancel: "Cancelar",
			current: "passo atual:",
			pagination: "Paginação",
			finish: "Cadastrar",
			next: "Avançar",
			previous: "Voltar",
			loading: "Aguarde ..."
		}
	});

	$('#curso').chosen({width: "100%"});

	$( ".indicacao" ).change(function() {

		if($('.indicacao').val() == 'consultor'){
			$("#listaAgenciador").css("display", "none");
			$("#listaColaborador").css("display", "block");
		}
		if($('.indicacao').val() == 'agenciador'){
			$("#listaColaborador").css("display", "none");
			$("#listaAgenciador").css("display", "block");
		}    
	});

	$( "#curso" ).change(function() {
		$("#categoriaCurso").val($(this).find(':selected').attr('data-categoria'));
	});

	$('a[href="#finish"]').click(function(){
		$("#wizard").submit();
	});

	$('a[href="#cancel"]').click(function(){
		location.reload();
	});
});