<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosPessoaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_pessoais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cpf', 80)->unique();
            $table->string('rg', 15)->nullable();
            $table->string('emissor', 15)->nullable();
            $table->date('nascimento')->nullable();;
            $table->string('nacionalidade', 30)->nullable();
            $table->string('naturalidade', 30)->nullable();
            $table->string('sexo', 10)->nullable();
            $table->string('pai')->nullable();
            $table->string('mae')->nullable();
            $table->timestamps();

            $table->unsignedinteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_pessoais');
    }
}
