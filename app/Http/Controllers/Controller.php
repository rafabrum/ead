<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Util\Constantes;
use App\Util\Util;
use Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\CategoriaMenu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public static function categoriasMenu(){
    	return CategoriaMenu::orderBy('ordem')->get();
    }

    /**
     * Trata diferentes respostas de requisição para retornar adequadamente
     * de acordo com cada tipo de requisição exibindo uma notificação
     * @param Request $request
     * @param array $dadosResposta
     * @param string $tituloMensagem
     * @param string $mensagem
     * @param string url $redirecionarPara
     * @param boolean $recarregar
     */
    protected function tratarRespostaComAlertaNotificacao($tituloMensagem, $mensagem, $redirecionarPara)
    {
        Util::exibirAlertaNotificacao($tituloMensagem, $mensagem, "success");

        return redirect($redirecionarPara);
    }
}
