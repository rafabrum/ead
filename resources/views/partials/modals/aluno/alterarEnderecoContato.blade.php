<div class="modal inmodal fade alterar-menu" id="modalAlterarEnderecoContato{{$aluno->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Endereço / Contato</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postAlterarEnderecoContatoAluno') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<input type="hidden" name="aluno_id" value="{{ $aluno->id }}" />
								<input type="hidden" name="user_id" value="{{ $aluno->user->id }}" />
								<div class="col-md-4">
									<div class="form-group">
										<label>Telefone</label>
										<input type="text" name="tel" value="{{$errors->has('tel') ? old('telefone') : $aluno->user->contato->tel}}" class="form-control">
										@if ($errors->has('tel'))
										<span class="help-block">
											<p>{{ $errors->first('tel') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Celular</label>
										<input type="text" name="cel" value="{{$errors->has('cel') ? old('celular') : $aluno->user->contato->cel}}" class="form-control">
										@if ($errors->has('cel'))
										<span class="help-block">
											<p>{{ $errors->first('cel') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>E-mail</label>
										<input type="text" required="" name="email" value="{{$errors->has('email') ? old('email') : $aluno->user->email}}" class="form-control">
										@if ($errors->has('email'))
										<span class="help-block">
											<p>{{ $errors->first('email') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>CEP</label>
										<input type="text" name="end_cep" value="{{$errors->has('end_cep') ? old('end_cep') : $aluno->user->endereco->end_cep}}" class="form-control">
										@if ($errors->has('end_cep'))
										<span class="help-block">
											<p>{{ $errors->first('end_cep') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Estado</label>
										<input type="text" name="end_estado" value="{{$errors->has('end_estado') ? old('end_estado') : $aluno->user->endereco->end_estado}}" class="form-control">
										@if ($errors->has('end_estado'))
										<span class="help-block">
											<p>{{ $errors->first('end_estado') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Cidade</label>
										<input type="text" name="end_cidade" value="{{$errors->has('end_cidade') ? old('cidade') : $aluno->user->endereco->end_cidade}}" class="form-control">
										@if ($errors->has('end_cidade'))
										<span class="help-block">
											<p>{{ $errors->first('end_cidade') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Bairro</label>
										<input type="text" name="end_bairro" value="{{$errors->has('end_bairro') ? old('bairro') : $aluno->user->endereco->end_bairro}}" class="form-control">
										@if ($errors->has('end_bairro'))
										<span class="help-block">
											<p>{{ $errors->first('end_bairro') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Rua</label>
										<input type="text" name="end_rua" value="{{$errors->has('end_rua') ? old('end_rua') : $aluno->user->endereco->end_rua}}" class="form-control">
										@if ($errors->has('end_rua'))
										<span class="help-block">
											<p>{{ $errors->first('end_rua') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Numero</label>
										<input type="text" name="end_numero" value="{{$errors->has('end_numero') ? old('numero') : $aluno->user->endereco->end_numero}}" class="form-control">
										@if ($errors->has('end_numero'))
										<span class="help-block">
											<p>{{ $errors->first('end_numero') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Complemento</label>
										<input type="text" name="Complemento" value="{{$errors->has('Complemento') ? old('Complemento') : $aluno->user->endereco->end_Complemento}}" class="form-control">
										@if ($errors->has('Complemento'))
										<span class="help-block">
											<p>{{ $errors->first('Complemento') }}</p>
										</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
