<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\Colaborador\IColaboradorRepo;
use App\Repositories\Usuario\IUsuarioRepo;
use Session;
use App\Repositories\Moodle\IMoodleRepo;
use App\Repositories\Aluno\IAlunoRepo;
use App\Repositories\Aluno\IAlunoCursoRepo;
use App\Repositories\DadosContatos\IDadosPessoaisRepo;
use App\Repositories\DadosContatos\IContatoRepo;
use App\Repositories\DadosContatos\IEnderecoRepo;
use App\Repositories\Agenciador\IAgenciadorRepo;
use App\Repositories\Encargo\IEncargoRepo;

use DB;

use App\Util\Constantes;

class CadastroUsuarioController extends Controller
{

    protected function novoUsuario($usuario, $request){

        return $usuario->criarUsuario($request);
    }

    public function novoAluno(IUsuarioRepo $usuario, IMoodleRepo $moodle, Request $request, IAlunoRepo $aluno, IAlunoCursoRepo $alunoCurso, IContatoRepo $contato, IEnderecoRepo $endereco, IDadosPessoaisRepo $dadosPessoais, IEncargoRepo $encargo){

      $this->validarDadosAluno($request);

      DB::beginTransaction();

      $request->password = $request->cpf;
      $novoUsuario = $this->novoUsuario($usuario, $request);

      $request->user_id = $novoUsuario->id;
      $request->senha_moodle = $request->cpf.'!aB.';

      $dadosMoodle = $this->tratarRequestAlunoMoodle($request);

      $cadastroMoodle = $moodle->criarUsuarioMoodle($dadosMoodle);
      $request->id_tipo_usuario_moodle = Constantes::ID_ALUNO_MOODLE;
      $objCadastroMoodle = json_decode($cadastroMoodle);

      if(array_key_exists(0, $objCadastroMoodle)){
        $request->moodle_id = $objCadastroMoodle[0]->id;
      }
      else{
        DB::rollBack();
        return "Ocorreu um erro ao tentar criar o aluno no moodle";
      }

      $cadastroAluno = $aluno->criarAluno($request);
      $request->aluno_id = $cadastroAluno->id;

      $moodle->MatricularcursoMoodle($request);

      if($request->colaborador_id != ""){
        $alunoCurso->criarAlunoCurso($request);
      }
      if($request->agenciador_id != ""){

      }

      $contato->criarContato($request);
      $dadosPessoais->criarDadosPessoais($request);
      $endereco->criarEndereco($request);
      $this->criarPlanoFinanceiro($request, $encargo);

      DB::commit();

      return $this->tratarRespostaComAlertaNotificacao( "OK!", "Aluno cadastrado com sucesso!", route('listaAluno'));

    }

    public function novoColaborador(IUsuarioRepo $usuario, IMoodleRepo $moodle, IColaboradorRepo $colaborador, Request $request){

      $this->validarDadosColaborador($request);

      $novoUsuario = $this->novoUsuario($usuario, $request);
      $request->user_id = $novoUsuario->id;
      $dadosMoodle = $this->tratarRequestColaboradorMoodle($request);
      $cadastroMoodle = $moodle->criarUsuarioMoodle($dadosMoodle);
      $objCadastroMoodle = json_decode($cadastroMoodle);
      $request->moodle_id = $objCadastroMoodle[0]->id;
      $tipoUsuarioMoodle = $moodle->definirTipoUsuarioMoodle($request);
      $dadosColaborador = $colaborador->tratarTipoUsuarioMoodle($request);
      $novoColaborador = $colaborador->criarColaborador($dadosColaborador);
        
      return $this->tratarRespostaComAlertaNotificacao( "OK!", "Colaborador cadastrado com sucesso!", route('listaColaborador'));
    }

    
    protected function criarPlanoFinanceiro($request, $encargo){

      if($request->vencimento_matricula == ""){
        $request->vencimento_matricula = date('Y-m-d');
      }

      if($request->valor_matricula != ""){
        $request->tipo_id = Constantes::TIPO_MATRICULA;
        $request->valor = $request->valor_matricula;
        $request->vencimento = $request->vencimento_matricula;
        $encargo->criarEncargo($request);
      }

      $i = 0;
      while ($i < $request->qtd_parcela){
        $request->tipo_id = Constantes::TIPO_PARCELA;
        $request->valor = $request->valor_parcela;
        $request->vencimento = $data = date('Y-m-d', strtotime("+".$i." month", strtotime($request->vencimento_parcela)));
        $encargo->criarEncargo($request);
        $i++;
      }
      
      return $encargo;

    }

    protected function tratarRequestColaboradorMoodle($request){

      return $dados = [
        'username' => $request->usuario_moodle,
        'password' => $request->password,
        'firstname' => $request->name,
        'lastname' => $request->sobrenome,
        'email' => $request->email,
        'auth' => 'manual',
        'idnumber' => $request->user_id,
        'timezone' => '-12.5',
        'mailformat' => 0,
        'city' => 'Belo Horizonte',
        'country' => 'br'
      ]; 
    }

    protected function tratarRequestAlunoMoodle($request){

      return $dados = [
        'username' => $request->cpf,
        'password' => $request->senha_moodle,
        'firstname' => $request->name,
        'lastname' => $request->sobrenome,
        'email' => $request->email,
        'auth' => 'manual',
        'idnumber' => $request->user_id,
        'timezone' => '-12.5',
        'mailformat' => 0,
        'city' => 'Belo Horizonte',
        'country' => 'br'
      ]; 
        
    }

    protected function validarDadosAluno($request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:40',
            'sobrenome' => 'required|string|min:3|max:40',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->user_id,
            'cpf' => 'required|min:9|unique:dados_pessoais|numeric',
            'curso_moodle_id' => 'required'
        ], [
            'required' => 'Campo obrigatório',
            'email.unique' => 'Já existe um usuário com esse email',
            'email.email' => 'O email precisa ser válido',
            'min' => 'Mínimo de 3 caracteres',
            'max' => 'Máximo de 40 caracteres',
            'cpf.min' => 'O cpf deve conter 11 caracteres',
            'cpf.unique' => 'Já existe no sistema um usuário com esse cpf'
        ]);
    }

    protected function validarDadosColaborador($request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:40',
            'sobrenome' => 'required|string|min:3|max:40',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->user_id,
            'password' => 'required|string|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[1-9][0-9])(?=.*[^a-zA-Z0-9_])/|confirmed',
            'usuario_moodle' => 'required|min:5|alpha_num|unique:colaboradores,usuario_moodle',
            'cpf' => 'required|min:9|unique:dados_pessoais|numeric',
        ], [
            'required' => 'Campo obrigatório',
            'email.unique' => 'Já existe um usuário com esse email',
            'email.email' => 'O email precisa ser válido',
            'min' => 'Mínimo de 3 caracteres',
            'max' => 'Máximo de 40 caracteres',
            'password.min' => 'Mínino de 8 caracteres',
            'password.confirmed' => 'As senhas digitadas não conferem',
            'alpha_num' => 'O usuario deve conter apenas letras e numeros',
            'regex' => 'A senha deve conter uma letra maiuscula um numero e um caracter especial',
            'cpf.min' => 'O cpf deve conter 11 caracteres',
            'cpf.unique' => 'Já existe no sistema um usuário com esse cpf'
        ]);
    }

 
}
