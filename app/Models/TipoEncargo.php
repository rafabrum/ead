<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEncargo extends Model
{
	protected $table = 'tipos_encargos';
	protected $primaryKey = 'id';

	protected $fillable = ["nome_encargo", "descricao"];

	public function encargo()
	{
		return $this->hasOne('App\Models\Encargo', "tipo_id", "id");
	}
}
