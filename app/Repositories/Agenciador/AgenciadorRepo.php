<?php
namespace App\Repositories\Agenciador;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Agenciador;

class AgenciadorRepo implements IAgenciadorRepo
{

	public function criarAgenciador($request){
		
		return Agenciador::create([
                'local' =>  $request->local,
                'status'  =>  $request->status, 
                'valor_fixo'  =>  $request->valor_fixo, 
                'capacitacao' => $request->capacitacao, 
                'percentual_comissao' => $request->percentual_comissao,
                'taxa_matricula' => $request->taxa_matricula,
                'percentual_matricula' => $request->percentual_matricula,
                'usuario_moodle' => $request->usuario_moodle,
                'senha_moodle' => $request->senha_moodle,
                'user_id' => $request->user_id
               ]);
	}
}
