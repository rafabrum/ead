<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Favicon -->
<link href="/favicon.ico" rel="shortcut icon" />
<!-- Latest compiled and minified CSS -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/css/animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/css/style.css') }}" rel="stylesheet" type="text/css">


@yield('styles-plugins')
@yield('styles')
<script>
    window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
</script>