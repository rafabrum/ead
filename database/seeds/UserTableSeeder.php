<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Desenvolvimento',
            'sobrenome' => 'Sistema',
            'email' => 'rasibrum@hotmail.com',
            'password' => Hash::make('abc123.'),
        ]);
    }
}
