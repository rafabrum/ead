<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('professores', function (Blueprint $table) {

                 $table->increments('id');
                 $table->timestamps();
                 $table->string('curriculo')->nullable();
                 $table->string('unidade', 50)->nullable();
                 $table->string('status', 30);
                 $table->string('foto')->nullable();
                 $table->string('titulacao', 60);
                 $table->integer('moodle_id')->nullable();
                 $table->string('usuario_moodle')->nullable();
                 $table->string('senha_moodle')->nullable();

                 $table->unsignedinteger('user_id');

                 $table->foreign('user_id')->references('id')->on('users');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professores');
    }
}
