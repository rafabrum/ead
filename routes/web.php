<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/teste', 'testeController@teste')->name('teste');

Route::get('/logout', 'Auth\LoginController@logout')->name('getLogout');

Route::get('/', function () {
	if(Auth::user()){
		return redirect('/home');
	}else{
		return view('/auth/login');
	}
});

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::get('/categorias-menus', 'PadraoController@viewCategoriasMenus')->name('categoriasMenus');

	Route::get('/home', 'HomeController@index')->name('home');

//=======================================Menus=================================================

	Route::group(['prefix' => 'menu'], function () {
		Route::get('/lista-menu', 'MenuController@viewListaMenu')->name('listaMenu');
		Route::post('/lista-menu', 'MenuController@viewListaMenu')->name('postListaMenu');
		Route::post('/alterar-menu', 'MenuController@alterarMenu')->name('postAlterarMenu');
		Route::post('/novo-menu', 'MenuController@novoMenu')->name('postNovoMenu');
		Route::post('/alterar-categoria', 'MenuController@alterarCategoria')->name('postAlterarCategoria');
		Route::post('/nova-categoria', 'MenuController@novaCategoria')->name('postNovaCategoria');
	});

	Route::group(['prefix' => 'curso'], function () {
		Route::any('/lista-curso', 'CursoController@viewListaCurso')->name('listaCurso');
	});

	Route::group(['prefix' => 'moodle'], function () {
		Route::any('/direcionar-moodle', 'MoodleController@acessoMoodle')->name('acessoMoodle');
	});

	Route::group(['prefix' => 'colaborador'], function () {

		Route::get('/lista-colaborador', 'ColaboradorController@viewListaColaborador')->name('listaColaborador');
		Route::post('/alterar-colaborador', 'ColaboradorController@alterarColaborador')->name('postAlterarColaborador');

		Route::get('/novo-colaborador', 'ColaboradorController@viewNovoColaborador')->name('viewNovoColaborador');
		Route::post('/novo-colaborador', 'CadastroUsuarioController@novoColaborador')->name('postNovoColaborador');
		
		Route::post('/alterar-permissoes', 'ColaboradorController@alterarPermissoesColaborador')->name('postAlterarPermissoesColaborador');
		Route::post('/alterar-senha', 'ColaboradorController@alterarSenhaColaborador')->name('postAlterarSenhaColaborador');
	});

	Route::group(['prefix' => 'aluno'], function () {
		Route::get('/novo-aluno', 'AlunoController@viewNovoAluno')->name('viewNovoAluno');
		Route::post('/novo-aluno', 'CadastroUsuarioController@novoAluno')->name('postNovoAluno');

		Route::post('/alterar-dados-pessoais', 'AlunoController@postAlterarDadosPessoaisAluno')->name('postAlterarDadosPessoaisAluno');
		Route::post('/alterar-endereco-contato', 'AlunoController@postAlterarEnderecoContatoAluno')->name('postAlterarEnderecoContatoAluno');

		Route::get('/lista-aluno', 'AlunoController@viewListaAluno')->name('listaAluno');
		Route::post('/lista-aluno', 'AlunoController@viewListaAluno')->name('postlistaAluno');
	});

});