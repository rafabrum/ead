<?php

namespace App\Repositories\Usuario;

interface IUsuarioRepo
{
	public function alterarUsuario($request);
	public function criarUsuario($request);
}
