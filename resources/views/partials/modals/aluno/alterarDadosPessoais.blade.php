<div class="modal inmodal fade alterar-menu" id="modalAlterarDadosPessoais{{$aluno->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Dados Pessoais</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postAlterarDadosPessoaisAluno') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<input type="hidden" name="aluno_id" value="{{ $aluno->id }}" />
								<input type="hidden" name="user_id" value="{{ $aluno->user->id }}" />
								<input type="hidden" name="email" value="{{ $aluno->user->email }}" />
								<div class="col-md-6">
									<div class="form-group">
										<label>Nome</label>
										<input type="text" required="" name="name" value="{{$errors->has('name') ? old('name') : $aluno->user->name}}" class="form-control">
										@if ($errors->has('name'))
										<span class="help-block">
											<p>{{ $errors->first('name') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Sobrenome</label>
										<input type="text" required="" name="sobrenome" value="{{$errors->has('sobrenome') ? old('sobrenome') : $aluno->user->sobrenome}}" class="form-control">
										@if ($errors->has('sobrenome'))
										<span class="help-block">
											<p>{{ $errors->first('sobrenome') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label>CPF</label>
										<input type="text" required="" name="cpf" value="{{$errors->has('cpf') ? old('cpf') : $aluno->user->dadosPessoais->cpf}}" class="form-control">
										@if ($errors->has('cpf'))
										<span class="help-block">
											<p>{{ $errors->first('cpf') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label>RG</label>
										<input type="text" name="rg" value="{{$errors->has('rg') ? old('rg') : $aluno->user->dadosPessoais->rg}}" class="form-control">
										@if ($errors->has('rg'))
										<span class="help-block">
											<p>{{ $errors->first('rg') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Emissor</label>
										<input type="text" name="emissor" value="{{$errors->has('emissor') ? old('emissor') : $aluno->user->dadosPessoais->emissor}}" class="form-control">
										@if ($errors->has('emissor'))
										<span class="help-block">
											<p>{{ $errors->first('emissor') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Nascimento</label>
										<input type="text" name="nascimento" value="{{$errors->has('nascimento') ? old('nascimento') : $aluno->user->dadosPessoais->nascimento}}" class="form-control">
										@if ($errors->has('nascimento'))
										<span class="help-block">
											<p>{{ $errors->first('nascimento') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Naturalidade</label>
										<input type="text" name="naturalidade" value="{{$errors->has('naturalidade') ? old('naturalidade') : $aluno->user->dadosPessoais->naturalidade}}" class="form-control">
										@if ($errors->has('naturalidade'))
										<span class="help-block">
											<p>{{ $errors->first('naturalidade') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Nacionalidade</label>
										<input type="text" name="nacionalidade" value="{{$errors->has('nacionalidade') ? old('nacionalidade') : $aluno->user->dadosPessoais->nacionalidade}}" class="form-control">
										@if ($errors->has('nacionalidade'))
										<span class="help-block">
											<p>{{ $errors->first('nacionalidade') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Sexo</label>
										<input type="text" name="sexo" value="{{$errors->has('sexo') ? old('sexo') : $aluno->user->dadosPessoais->sexo}}" class="form-control">
										@if ($errors->has('sexo'))
										<span class="help-block">
											<p>{{ $errors->first('sexo') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label>Pai</label>
										<input type="text" name="pai" value="{{$errors->has('pai') ? old('pai') : $aluno->user->dadosPessoais->pai}}" class="form-control">
										@if ($errors->has('pai'))
										<span class="help-block">
											<p>{{ $errors->first('pai') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label>Mãe</label>
										<input type="text" name="mae" value="{{$errors->has('mae') ? old('mae') : $aluno->user->dadosPessoais->mae}}" class="form-control">
										@if ($errors->has('mae'))
										<span class="help-block">
											<p>{{ $errors->first('mae') }}</p>
										</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
