<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('partials.head-pagina')
    </head>

    <body class="gray-bg">

        @include('partials.loading-page')

        <!-- Main view  -->
        @yield('content')

        <script src="{{ elixir('assets/template/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ elixir('assets/template/js/bootstrap.min.js') }}"></script>
        <script src="{{ elixir('assets/template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ elixir('assets/template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        @yield('scripts-plugins')
        @yield('scripts')

    </body>
</html>
