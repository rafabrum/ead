<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encargo extends Model
{
    	protected $table = 'encargos';
    protected $primaryKey = 'id';

    protected $fillable = ["vencimento", "valor",
						  "tipo_id", "aluno_id", "curso_id"];

	public function tipo()
    {
         return $this->belongsTo('App\User', "tipo_id", "id");
    }
}
