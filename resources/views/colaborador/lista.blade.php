@extends('layouts.app')
@section('styles')
<link href="{{ elixir('assets/template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>Lista de colaboradores</h2>
    </div>
</div>
<div id="recuperarModal" data-modal="modalAlterar{{Session::get('modal')}}"></div>
<div id="recuperarModalSenha" data-modal="modalNovaSenha{{Session::get('modalSenha')}}"></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datatables-lista">
                            <thead>
                                <tr>
                                    <th>Colaborador</th>
                                    <th class="col-lg-1">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($colaboradores as $colaborador)
                                <tr class="gradeX">
                                    <td>{{$colaborador->user->name}} {{$colaborador->user->sobrenome}}</td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Opções <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal" data-target="#modalAlterar{{$colaborador->id}}" href="" title="Alterar"><i></i>Alterar</a></li>
                                                <li><a data-toggle="modal" data-target="#modalPermissoes{{$colaborador->id}}" href="" title="Permissões"><i></i>Permissoes</a></li>
                                                <li><a data-toggle="modal" data-target="#modalNovaSenha{{$colaborador->id}}" href="" title="Nova Senha"><i></i>Alterar Senha</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @include('partials.modals.colaborador.nova-senha', ['colaborador' => $colaborador])
                                @include('partials.modals.colaborador.permissoes', ['colaborador' => $colaborador, 'menus' => $menus])
                                @include('partials.modals.colaborador.alterar', ['colaborador' => $colaborador])
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Colaborador</th>
                                    <th>Ações</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ elixir('assets/template/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ elixir('assets/js/datatables-lista.js') }}"></script>
@if(Session::has('errors') && Session::has('modal'))
<script type="text/javascript">
    $(document).ready(function () {
        var modal = $('#recuperarModal').data().modal;       
        $('#'+ modal).modal('show');
    });
</script>
@elseif(Session::has('errors') && Session::has('modalSenha'))
<script type="text/javascript">
    $(document).ready(function () {
        var modalSenha = $('#recuperarModalSenha').data().modal;       
        $('#'+ modalSenha).modal('show');
    });
</script>
@endif
@endsection