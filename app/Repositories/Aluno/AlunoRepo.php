<?php
namespace App\Repositories\Aluno;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Aluno;

class AlunoRepo implements IAlunoRepo
{

	public function criarAluno($request){
		
		return Aluno::create([
                'formacao' =>  $request->formacao,
                'instituicao'  =>  $request->instituicao, 
                'ano_formacao'  =>  $request->ano_formacao, 
                'status' => 'bloqueado',
                'moodle_id' => $request->moodle_id,
                'senha_moodle' => $request->senha_moodle,
                'usuario_moodle' => $request->usuario_moodle,
                'user_id' => $request->user_id,
               ]);
	}

    public function criarAluno($request){
        
        return Aluno::where('id', $request->aluno_id)
              ->update([
                'formacao' =>  $request->formacao,
                'instituicao'  =>  $request->instituicao, 
                'ano_formacao'  =>  $request->ano_formacao, 
                'status' => 'bloqueado',
                'moodle_id' => $request->moodle_id,
                'senha_moodle' => $request->senha_moodle,
                'usuario_moodle' => $request->usuario_moodle,
                'user_id' => $request->user_id, 
               ]);
    }
}
