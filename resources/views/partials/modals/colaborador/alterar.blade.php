<div class="modal inmodal fade alterar-menu" id="modalAlterar{{$colaborador->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Dados do Colaborador</h4>
			</div>
			<form role="form" class="form-menu" method="POST" action="{{ route('postAlterarColaborador') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<input type="hidden" name="colaborador_id" value="{{ $colaborador->id }}" />
								<input type="hidden" name="moodle_id" value="{{ $colaborador->moodle_id }}" />
								<input type="hidden" name="user_id" value="{{ $colaborador->user->id }}" />
								<div class="col-md-6">
									<div class="form-group">
										<label>Nome</label>
										<input type="text" required="" name="name" value="{{$errors->has('name') ? old('name') : $colaborador->user->name}}" class="form-control">
										@if ($errors->has('name'))
										<span class="help-block">
											<p>{{ $errors->first('name') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Sobrenome</label>
										<input type="text" required="" name="sobrenome" value="{{$errors->has('sobrenome') ? old('sobrenome') : $colaborador->user->sobrenome}}" class="form-control">
										@if ($errors->has('sobrenome'))
										<span class="help-block">
											<p>{{ $errors->first('sobrenome') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Usuario Moodle</label>
										<input type="text" name="usuario_moodle" value="{{$errors->has('usuario_moodle') ? old('setor') : $colaborador->usuario_moodle}}" class="form-control">
										@if ($errors->has('usuario_moodle'))
										<span class="help-block">
											<p>{{ $errors->first('usuario_moodle') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Tipo Usuario Moodle</label>
										<select name="id_tipo_usuario_moodle" class="form-control">
											<option value="{{$colaborador->id_tipo_usuario_moodle}}">{{$colaborador->tipo_usuario_moodle}}</option>
											@if($colaborador->id_tipo_usuario_moodle != 7)
											<option value="7">Acesso Basico</option>
											@endif
											@if($colaborador->id_tipo_usuario_moodle != 4)
											<option value="4">Moderador</option>
											@endif
											@if($colaborador->id_tipo_usuario_moodle != 2)
											<option value="2">Criador de cursos</option>
											@endif
											@if($colaborador->id_tipo_usuario_moodle != 3)
											<option value="3">Professor</option>
											@endif
											@if($colaborador->id_tipo_usuario_moodle != 1)
											<option value="1">Gerente</option>
											@endif
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>E-mail</label>
										<input type="text" required="" name="email" value="{{$errors->has('email') ? old('email') : $colaborador->user->email}}" class="form-control">
										@if ($errors->has('email'))
										<span class="help-block">
											<p>{{ $errors->first('email') }}</p>
										</span>
										@endif
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Ramal</label>
										<input type="text" name="ramal" value="{{$errors->has('ramal') ? old('ramal') : $colaborador->ramal}}" class="form-control">
										@if ($errors->has('ramal'))
										<span class="help-block">
											<p>{{ $errors->first('ramal') }}</p>
										</span>
										@endif
									</div>

								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Status</label>
										<select name="status" class="form-control">
											<option value="{{$colaborador->status}}">{{$colaborador->status}}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>
