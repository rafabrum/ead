@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>Novo Colaborador</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <form id="wizard" class="wizard-big course-finder-form form-atendimento" method="POST" action="{{ route('postNovoColaborador') }}">
                {{ csrf_field() }}

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" required="" name="name" value="{{old('name')}}" placeholder="exemplo: João" class="form-control">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <p>{{ $errors->first('name') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Sobrenome</label>
                                <input type="text" required="" name="sobrenome" placeholder="exemplo: da Silva" value="{{old('sobrenome')}}" class="form-control">
                                @if ($errors->has('sobrenome'))
                                <span class="help-block">
                                    <p>{{ $errors->first('sobrenome') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>usuario Moodle</label>
                                <input type="text" required="" name="usuario_moodle" placeholder="exemplo: joaosilva" value="{{old('usuario_moodle')}}" class="form-control">
                                @if ($errors->has('usuario_moodle'))
                                <span class="help-block">
                                    <p>{{ $errors->first('usuario_moodle') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tipo Usuario Moodle</label>
                                <select name="id_tipo_usuario_moodle" class="form-control">
                                    <option value="7">Acesso Basico</option>
                                    <option value="4">Moderador</option>
                                    <option value="2">Criador de cursos</option>
                                    <option value="3">Professor</option>
                                    <option value="1">Gerente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" required="" name="email" placeholder="exemplo: joao@dominio.com" value="{{old('email')}}" class="form-control">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <p>{{ $errors->first('email') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Ramal</label>
                                <input type="text" name="ramal" value="{{old('ramal')}}" placeholder="exemplo:500" class="form-control">
                                @if ($errors->has('ramal'))
                                <span class="help-block">
                                    <p>{{ $errors->first('ramal') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="ativo">ativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Senha</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <p>{{ $errors->first('password') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password-confirm" class="col-lg-6 col-form-label text-md-right">Confirme a senha</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
            </form>


        </div>
    </div>
</div>

@endsection