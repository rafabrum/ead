<?php
namespace App\Util;

use Session;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Log;
use Validator;
use Cache;

class Util
{

    /**
     * Método retorna somente caracteres numéricos de uma string
     * @param string $string
     * @return string
     */
    public static function obterSomenteNumerosDaString($string)
    {
        if (empty($string)) {
            return "";
        }
        return preg_replace("/[^0-9]/", "", $string);
    }

    /**
     *
     * @param type $mensagem Texto ou Blade(html)
     * @param type $tipoMensagem Tipo da mensagem, Ex. danger, info
     * @param type $tipoAlerta Tipo do alerta Constantes::TIPO_ALERTA_SUPERIOR
     * @param type $dadosAlerta
     */
    public static function exibirAlerta($mensagem, $tipoMensagem, $tipoAlerta, $modal = false, $sweetAlerta = false, $tituloMensagem = "Sucesso!", $dadosAlerta = null)
    {
        if ($sweetAlerta) {
            Session::flash('exibirSweetAlerta', [
                'titulo' => $tituloMensagem,
                'mensagem' => $mensagem,
                'tipoAlerta' => $tipoMensagem,
                'dadosAlerta' => $dadosAlerta
            ]);

            return;
        }

        if ($modal) {
            flash()->overlay($mensagem, "Mensagem");
            return;
        }

        flash($mensagem, ['tipoMensagem' => $tipoMensagem, 'tipoAlerta' => $tipoAlerta]);
    }

    /**
     *
     * @param string $tituloMensagem Título da mensagem
     * @param string $mensagem Mensagem com a pergunta
     * @param string $urlCalback caminho para requisição Ajax após confirmação
     */
    public static function exibirDialogoAlerta($tituloMensagem, $mensagem, $textoBotaoConfirmacao, $urlCallback, $urlCallbackCancela)
    {
        Session::flash('exibirDialogoSweetAlerta', [
            'titulo' => $tituloMensagem,
            'mensagem' => $mensagem,
            'textoBotaoConfirmação' => $textoBotaoConfirmacao,
            'urlCallback' => $urlCallback,
            'urlCallbackCancela' => $urlCallbackCancela
        ]);
        return;
    }

    /**
     *
     * @param string $tituloMensagem
     * @param string $mensagem
     * @param string $tipo
     */
    public static function exibirAlertaNotificacao($tituloMensagem = "Sucesso!", $mensagem = "Dados atualizados.", $tipo = "success")
    {
        Session::flash('exibirAlertaNotificacao', [
            'titulo' => $tituloMensagem,
            'mensagem' => $mensagem,
            'tipo' => $tipo
        ]);

        return;
    }

    public static function transformarPaginacaoApiParaWeb($objPaginacao)
    {

        $dadosPaginados = new LengthAwarePaginator($objPaginacao->data, $objPaginacao->total, $objPaginacao->per_page);
        $url = \Request::url();
        if (\Request::has('q')) {
            $url .= "?q=" . \Request::get('q');
        }
        $dadosPaginados->setPath($url);

        return $dadosPaginados;
    }

    public static function transformarCollectItemsParaPaginacaoWeb($iems, $page = 1)
    {

        $dadosPaginados = new LengthAwarePaginator($iems, $iems->count(), $page);
        $url = \Request::url();
        if (\Request::has('q')) {
            $url .= "?q=" . \Request::get('q');
        }
        $dadosPaginados->setPath($url);

        return $dadosPaginados;
    }

    public static function ehObjetoPaginacao($objPaginacao)
    {
        return $objPaginacao instanceof LengthAwarePaginator;
    }

    public static function preparaFiltroRequisicaoPesquisaPorQuery($request, $filtro = [], $camposPesquisaveis = [])
    {
        if (!$request->has("q") || empty($request->get("q")) || empty($filtro) || empty($camposPesquisaveis)) {
            array_forget($filtro, "q");
            return $filtro;
        }
        array_set($filtro, "q", $request->get("q"));
        array_set($filtro, "qFields", implode(",", $camposPesquisaveis));
        return $filtro;
    }

    public static function processouCorretamente($respostaRequisicao)
    {
        if ($respostaRequisicao === true) {
            return true;
        }

        return ($respostaRequisicao['resposta']->httpCode == Response::HTTP_OK || $respostaRequisicao['resposta']->httpCode == Response::HTTP_CREATED);
    }

    /**
     * Método informa se a rota equivale a url informada ou url atual
     * caso url seja null.
     * @param string $rota
     * @param string $url
     * @return type
     */
    public static function urlEquivaleRota($rota, $url = null)
    {
        if (!$url) {
            $url = url();
        }

        return str_contains($url, $rota);
    }

    /**
     * Em alguns casos preciso limpar o token para gerar um novo
     */
    public static function limparTokenApi()
    {
        Session::forget(Constantes::SESSAO_TOKEN_API);
    }

    public static function formatarValorMonetarioExibicao($floatNumber, $prefixo = "")
    {
        return $prefixo . number_format((double) $floatNumber, 2, ',', '.');
    }

    public static function formatarValorMonetarioDecimal($valor)
    {
        if (!$valor) {
            return $valor;
        }

        return $valor / 100;
    }

    public static function formatarTelefoneExibicao($numero)
    {
        $novo = substr_replace($numero, '(', 0, 0);
        $novo = substr_replace($novo, ') ', 3, 0);
        return $novo;
    }

    public static function rolarParaConteudoDaPaginaAposCarregamento($idHtml)
    {
        Session()->flash('rolarParaConteudoId', $idHtml);
    }

    /**
     * Registra uma informação de log
     * @param type $identificador
     * @param type $rotina
     * @param type $mensagem
     * @param type $tipo (Info = 1, error = 2)
     */
    public static function registrarRotinaNoLog($identificador, $rotina, $mensagem, $tipo = 1)
    {
        if ($tipo == 1) {
            Log::info($identificador, ['rotina' => $rotina, 'mensagem' => $mensagem]);
        } else {
            Log::error($identificador, ['rotina' => $rotina, 'mensagem' => $mensagem]);
        }
    }

    public static function removerCaracteresOcultos($txt)
    {
        $retorno = Cache::remember('patternsUnicodeDecode', Util::MINUTOS_CACHE, function () {
                $patterns = [];
                $replacements = [];
                for ($i = 1; $i < 32; $i++) {
                    $patterns += [$i => "/" . chr($i) . "/"];
                    if ($i == 10) {
                        $replacements += [$i => chr($i)];
                    } else {
                        $replacements += [$i => ''];
                    }
                }
                return ['patterns' => $patterns, 'replacements' => $replacements];
            });

        $txt = preg_replace($retorno['patterns'], $retorno['replacements'], $txt);
        $txt = preg_replace('/%u([[:alnum:]]{4})/', '-', $txt);
        return $txt;
    }

    public static function getUtm($inicializarCom = "?", $utm_source, $utm_medium, $utm_campaign)
    {
        return sprintf("%sutm_source=%s&utm_medium=%s&utm_campaign=%s", $inicializarCom, $utm_source, $utm_medium, $utm_campaign);
    }

    public static function ehEmailValido($email)
    {
        if (empty($email)) {
            return false;
        }

        $dados['email'] = $email;
        $validator = Validator::make($dados, [
                'email' => 'email',
        ]);

        return !$validator->fails();
    }

    public static function ehUrl($text)
    {
        return filter_var($text, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) !== false;
    }

    public static function getUrlFoto($foto)
    {
        return self::ehUrl($foto) ? $foto : route('getFoto', [str_replace(".png", "", $foto)]);
    }

    public static function removerAcentos($textoComAcento)
    {
        $comAcento = mb_split("\s", 'á à ã â é ê ì î í ó ô õ ú ü ç ñ Á À Ã Â É Ê Ì Î Í Ó Ô Õ Ú Ü Ç Ñ');
        $semAcento = str_split('aaaaeeiiiooouucnAAAAEEIIIOOOUUCN');

        $textoSemAcento = strtr($textoComAcento, array_combine($comAcento, $semAcento));

        return $textoSemAcento;
    }

    public static function stringContemNumero($string)
    {
        return is_numeric(filter_var($string, FILTER_SANITIZE_NUMBER_INT));
    }

    /**
     * Retorna a senha criptografada nos padroes do codigo velho
     * @param string $senha
     * @return string
     */
    public static function obterSenhaCriptografadaCodigoVelho($senha)
    {
        return base64_encode(md5($senha));
    }

    public static function cryptData($data)
    {
        return base64_encode($data);
    }

    public static function decryptData($data)
    {
        return base64_decode($data);
    }

    public static function cryptPath($path)
    {
        return env('PORTAL_COLABORADOR') . "/?m=" . base64_encode($path);
    }

    public static function getCoresGraficas()
    {
        return ['#058DC7', '#55C6A1', '#BA78FF', '#ff3d7b', '#87888A', '#FFF176', '#EA2462', '#ED5565'];
    }
    public static function getMeses($numerico = false)
    {
        if ($numerico) {
            return ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        }
        return ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dec'];
    }

    /**
     * retorna um array com quantidades de determinada coleção agrupados por mês
     * sem posição vazia
     * @param \Illuminate\Support\Collection $colecao
     */
    public static function obterQuantidadeAgrupadaPorMesDeUmaColecao($colecao, $campodata = "created_at")
    {
        $quantidadesAgrupadasPorMes = [];
        $meses = self::getMeses(true);
        $dadosAgrupadosPorMes = $colecao->sortBy($campodata)->groupBy(function($item) use($campodata) {
            return \Carbon\Carbon::parse($item->$campodata)->format('m');
        });
        foreach ($meses as $value) {
            $quantidadesAgrupadasPorMes[$value] = isset($dadosAgrupadosPorMes[$value]) ? $dadosAgrupadosPorMes[$value]->count() : 0;
        }
        return $quantidadesAgrupadasPorMes;
    }
}
