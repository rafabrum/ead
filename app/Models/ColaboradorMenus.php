<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ColaboradorMenus extends Model
{
	use SoftDeletes;


	protected $table = 'colaborador_menus';
	protected $primaryKey = 'id';
	protected $fillable = [
		'colaborador_id',
		'menu_id',
	];

	protected $dates = ['deleted_at']; 	
}
