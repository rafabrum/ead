<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlunoCurso extends Model
{
	protected $table = 'alunos_cursos';
	protected $primaryKey = 'id';

	protected $fillable = ["curso_moodle_id", "curso_moodle_categoria_id","aluno_id", "consultor_id"];


	public function aluno()
	{
		return $this->belongsTo('App\Models\Aluno', "aluno_id", "id");
	}

}
