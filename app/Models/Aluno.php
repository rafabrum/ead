<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
	protected $table = 'alunos';
    protected $primaryKey = 'id';
    
	protected $fillable = ["formacao", "instituicao", "ano_formacao",
							"status", "moodle_id", "user_id", "senha_moodle", "usuario_moodle"];

	public function user()
    {
        return $this->belongsTo('App\User', "user_id", "id");
    }

   	public function curso()
    {
        return $this->hasMany('App\Models\AlunoCurso', "id", "aluno_id");
    }

}
