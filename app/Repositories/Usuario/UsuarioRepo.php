<?php
namespace App\Repositories\Usuario;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsuarioRepo implements IUsuarioRepo
{

	public function alterarUsuario($request){

		return User::where('id', $request->user_id)
              ->update([
                'name' =>  $request->name,
                'sobrenome'  =>  $request->sobrenome, 
                'email'  =>  $request->email, 
               ]);
	}

	public function criarUsuario($request){
		
		return User::create([
                'name' =>  $request->name,
                'sobrenome'  =>  $request->sobrenome, 
                'email'  =>  $request->email, 
                'password' => Hash::make($request->password)
               ]);
	}
}
