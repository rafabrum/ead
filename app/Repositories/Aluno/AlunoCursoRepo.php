<?php
namespace App\Repositories\Aluno;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\AlunoCurso;

class AlunoCursoRepo implements IAlunoCursoRepo
{

	public function criarAlunoCurso($request){
		
		return AlunoCurso::create([
                'curso_moodle_id' =>  $request->curso_moodle_id,
                'aluno_id'  =>  $request->aluno_id, 
                'consultor_id'  =>  $request->colaborador_id,
                'curso_moodle_categoria_id' =>  $request->curso_moodle_categoria_id,
               ]);
	}
}
