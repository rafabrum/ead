<?php
namespace App\Repositories\Colaborador;

use Illuminate\Http\Request;
use App\Models\Colaborador;
use App\User;
use Illuminate\Support\Facades\Crypt;

class ColaboradorRepo implements IColaboradorRepo
{

  public function criarColaborador($request){
    return Colaborador::create([
      'ramal' => $request->ramal,
      'status' => $request->status,
      'formacao' => $request->formacao,
      'moodle_id' => $request->moodle_id,
      'usuario_moodle' => mb_strtolower($request->usuario_moodle,'UTF-8'),
      'senha_moodle' => base64_encode($request->password),
      'tipo_usuario_moodle' => $request->tipo_usuario_moodle,
      'id_tipo_usuario_moodle' => $request->id_tipo_usuario_moodle,
      'user_id' => $request->user_id
    ]);
  }

	public function alterarColaborador($request){

		return Colaborador::where('id', $request->colaborador_id)
              ->update([
                'ramal'  =>  $request->ramal, 
                'status'  =>  $request->status,	
        				'formacao'  =>  $request->formacao, 
        				'moodle_id'  =>  $request->moodle_id,
                'usuario_moodle' => $request->usuario_moodle,
                'tipo_usuario_moodle' => $request->tipo_usuario_moodle,
                'id_tipo_usuario_moodle' => $request->id_tipo_usuario_moodle, 
               ]);
	}

  public function tratarTipoUsuarioMoodle($request){

              switch ($request->id_tipo_usuario_moodle) {
                case 0:
                    $tipoUsuarioMoodle = "admin";
                    break;
                case 1:
                     $tipoUsuarioMoodle = "gerente";
                    break;
                case 2:
                     $tipoUsuarioMoodle = "criador de cursos";
                    break;
                case 3:
                     $tipoUsuarioMoodle = "professor";
                    break;
                case 4:
                     $tipoUsuarioMoodle = "moderador";
                    break;
                case 7:
                     $tipoUsuarioMoodle = "acesso basico";
                    break;
          }

        $request->tipo_usuario_moodle = $tipoUsuarioMoodle;

        return $request;
  }
}
