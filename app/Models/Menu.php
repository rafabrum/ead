<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table = 'menus';
    protected $primaryKey = 'id';

    protected $fillable = ["titulo", "alias", "url", "categoria_id"];

    public function colaborador()
    {
        return $this->belongsToMany('App\Models\Colaborador','colaborador_menus', 'menu_id','colaborador_id');
                    
    }

   	public function categoria()
    {
        return $this->belongsTo('App\Models\CategoriaMenu', "categoria_id", "id");
    }
}
