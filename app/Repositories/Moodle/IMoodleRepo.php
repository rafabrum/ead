<?php
namespace App\Repositories\Moodle;

interface IMoodleRepo
{

    /**
     * Método retorna os menus existentes do portal
     * @return array
     */

    public function criarUsuarioMoodle($dados);

    public function matricularCursoMoodle($dados);

    public function definirTipoUsuarioMoodle($request);

    public function removerTipoUsuarioMoodle($tipoId, $moodleId);

    public function listarCursosMoodle();

    public function listarCategoriasCursosMoodle();

    public function getCategoriaMoodle($categoriaId);

}