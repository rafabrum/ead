<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenciadores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('local', 60);
            $table->string('status', 30);
            $table->decimal('valor_fixo');
            $table->boolean('capacitacao');
            $table->integer('percentual_comissao');
            $table->boolean('taxa_matricula');
            $table->integer('percentual_matricula');
            $table->integer('moodle_id')->nullable();
            $table->string('usuario_moodle')->nullable();
            $table->string('senha_moodle')->nullable();
            $table->unsignedinteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenciadores');
    }
}
