<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = ["curriculo", "unidade", "status", "foto",
							"titulacao", "moodle_id", "user_id"];

	public function user()
    {
         return $this->belongsTo('App\User', "id", "user_id");
    }
}
