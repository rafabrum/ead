<?php

namespace App\Repositories\Aluno;

interface IAlunoCursoRepo
{
	public function criarAlunoCurso($request);
}
