<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos_cursos', function (Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->unsignedinteger('curso_moodle_id');
            $table->unsignedinteger('curso_moodle_categoria_id');
            $table->unsignedinteger('aluno_id');
            $table->integer('consultor_id')->nullable();

            $table->foreign('aluno_id')->references('id')->on('alunos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos_cursos');
    }
}
