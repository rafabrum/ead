<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{Auth::user()->name}}</strong>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ route('getLogout') }}">Sair</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        EAD
                    </div>
                </li>
                @foreach(App\Http\Controllers\Controller::CategoriasMenu() as $categoria)
                <li>
                    <a href="index.html"><i class="{{$categoria->icone}}"></i> <span class="nav-label"> {{$categoria->nome}}</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @foreach(Auth::user()->colaborador->menus as $menu)
                            @if($menu->categoria_id == $categoria->id)
                            <li><a href="{{ route(''.$menu->url.'') }}">{{$menu->titulo}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                 @endforeach
            </ul>
        </div>
    </nav>
