@extends('layouts.app')
@section('styles')
<link href="{{ elixir('assets/template/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet" type="text/css">
<link href="{{ elixir('assets/template/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
.wizard-big.wizard > .content {
    min-height: 350px;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>Novo Aluno</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <form id="wizard" class="wizard-big course-finder-form form-atendimento" action="{{ route('postNovoAluno') }}" method="POST">
                        {{ csrf_field() }}

                        <h1>Dados pessoais</h1>
                        <div class="step-content">
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Nome *</label>
                                    <input id="nome" name="name" type="text" class="form-control" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Sobrenome *</label>
                                    <input id="sobrenome" name="sobrenome" type="text" class="form-control" value="{{old('sobrenome')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>cpf</label>
                                    <input id="cpf" name="cpf" type="text" class="form-control" value="{{old('cpf')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>RG</label>
                                    <input id="rg" name="rg" type="text" class="form-control" value="{{old('rg')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>Nascimento</label>
                                    <input id="nascimento" name="nascimento" type="date" class="form-control" value="{{old('nascimento')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>Sexo *</label>
                                    <input id="sexo" name="sexo" type="text" class="form-control" value="{{old('sexo')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>Naturalidade *</label>
                                    <input id="naturalidade" name="naturalidade" type="text" class="form-control" value="{{old('naturalidade')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>Nacionalidade *</label>
                                    <input id="nacionalidade" name="nacionalidade" type="text" class="form-control" value="{{old('nacionalidade')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Pai *</label>
                                    <input id="pai" name="pai" type="text" class="form-control" value="{{old('pai')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Mãe *</label>
                                    <input id="mae" name="mae" type="text" class="form-control" value="{{old('mae')}}">
                                </div>
                            </div>
                        </div>

                        <h1>Contato / Endereço</h1>
                        <div class="step-content">
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>E-mail*</label>
                                    <input id="email" name="email" type="text" class="form-control" value="{{old('email')}}">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group ">
                                    <label>Telefone *</label>
                                    <input id="tel" name="tel" type="text" class="form-control" value="{{old('tel')}}">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group ">
                                    <label>Celular *</label>
                                    <input id="cel" name="cel" type="text" class="form-control" value="{{old('cel')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Cidade *</label>
                                    <input id="cidade" name="end_cidade" type="text" class="form-control" value="{{old('cidade')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group ">
                                    <label>Bairro *</label>
                                    <input id="bairro" name="end_bairro" type="text" class="form-control" value="{{old('bairro')}}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group ">
                                    <label>CEP *</label>
                                    <input id="cep" name="end_cep" type="text" class="form-control" value="{{old('cep')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group ">
                                    <label>Rua *</label>
                                    <input id="rua" name="end_rua" type="text" class="form-control" value="{{old('rua')}}">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group ">
                                    <label>Número *</label>
                                    <input id="numero" name="end_numero" type="text" class="form-control" value="{{old('numero')}}">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group ">
                                    <label>Complemento *</label>
                                    <input id="complemento" name="end_complemento" type="text" class="form-control" value="{{old('complemento')}}">
                                </div>
                            </div>
                        </div>

                        <h1>Dados acadêmicos</h1>
                        <div class="step-content">
                            <div class="col-lg-12" style="margin-bottom: 20px;">
                                <label>Curso</label>
                                <div>
                                    <select data-placeholder="cursos" id="curso" name="curso_moodle_id" class="form-control" tabindex="2">
                                        <option value="">Selecione</option>
                                        @foreach($cursos as $curso)
                                        @if($curso->id != 1)
                                        <option data-categoria="{{$curso->categoryid}}" value="{{$curso->id}}">{{$curso->displayname}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="categoriaCurso" name="curso_moodle_categoria_id" value="">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Formação *</label>
                                    <input id="formacao" name="formacao" type="text" class="form-control" value="{{old('formacao')}}">
                                </div>
                                <div class="form-group">
                                    <label>Instituição *</label>
                                    <input id="instituicao" name="instituicao" type="text" class="form-control" value="{{old('instituicao')}}">
                                </div>
                                <div class="form-group">
                                    <label>Ano de Conclusão</label>
                                    <input id="ano_formacao" name="ano_formacao" type="text" class="form-control" value="{{old('ano_formacao')}}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="text-center">
                                    <div style="margin-top: 20px">
                                        <i class="fa fa-mortar-board" style="font-size: 180px;color: #e5e5e5 "></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1>Financeiro</h1>
                        <div class="step-content">
                            <div class="col-lg-6">
                                <label>Tipo de venda</label>
                                <div>
                                    <select class="form-control indicacao" required="">
                                        <option value="">Selecione</option>
                                        <option value="consultor">Consultor</option>
                                        <option value="agenciador">Agenciador</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6" id="listaColaborador">
                                <label >Colaborador</label>
                                <div>
                                    <select name="colaborador_id" class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach($colaboradores as $colaborador)
                                        <option value="{{$colaborador->id}}">{{$colaborador->user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6" id="listaAgenciador">
                                <label >Agenciador</label>
                                <div>
                                    <select name="agenciador_id" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="">teste</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="margin-top: 40px;">
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label>Valor matrícula*</label>
                                            <input id="valorMatricula" name="valor_matricula" type="text" class="form-control" value="{{old('valor_matricula')}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label>Vencimento matricula*</label>
                                            <input id="vencimentoMatricula" name="vencimento_matricula" type="date" class="form-control" value="{{old('vencimentoMatricula')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="margin-top: 40px;">

                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label>Valor parcela*</label>
                                            <input id="valorParcela" name="valor_parcela" type="text" class="form-control" value="{{old('valor_parcela')}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label>Primeiro Vencimento*</label>
                                            <input id="vencimentoParcela" name="vencimento_parcela" type="date" class="form-control" value="{{old('vencimento_parcela')}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label>Quantidade *</label>
                                            <input id="qtdParcela" name="qtd_parcela" type="text" class="form-control" value="{{old('qtd_parcela')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('partials.modals.aluno.validacao')
</div>
@endsection
@section('scripts-plugins')
<script src="{{ elixir('assets/template/js/plugins/staps/jquery.steps.min.js') }}"></script>
<script src="{{ elixir('assets/template/js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ elixir('assets/js/novo-aluno.js') }}"></script>
@if(Session::has('errors'))
<script type="text/javascript">
    $(document).ready(function () {   
        $('#modalValidacaoAluno').modal('show');
    });
</script>
@endif
@endsection