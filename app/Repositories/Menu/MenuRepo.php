<?php
namespace App\Repositories\Menu;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\CategoriaMenu;
use App\User;

class MenuRepo implements IMenuRepo
{


  public function criarMenu($request){

    return Menu::create([
        'titulo' => $request->titulo,
        'alias' => $request->alias,
        'url' => $request->url,
        'categoria_id' => $request->categoria_id
      ]);
  }

  public function alterarMenu($request){

    return   Menu::where('id', $request->menu_id)
              ->update([
                'titulo' => $request->titulo,
                'alias' => $request->alias,
                'url' => $request->url,
                'categoria_id' => $request->categoria_id
               ]);
  }

  public function criarCategoria($request){

    return CategoriaMenu::create([
        'nome' => $request->nome,
        'ordem' => $request->ordem,
        'icone' => $request->icone      
      ]);
  }

  public function alterarCategoria($request){

    return CategoriaMenu::where('id', $request->categoria_id)
                      ->update([
                        'nome' => $request->nome,
                        'ordem' => $request->ordem,
                        'icone' => $request->icone
                       ]);
  }

}
