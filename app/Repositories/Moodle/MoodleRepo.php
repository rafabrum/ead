<?php
namespace App\Repositories\Moodle;

class MoodleRepo implements IMoodleRepo
{

      private $token;
      private $domainName;
      protected $url;

      public function __construct(){

        $this->token = env('TOKEN_MOODLE');
        $this->domainName = env('DOMINIO_MOODLE');
        
        $this->url = $this->domainName . '/webservice/rest/server.php' . '?wstoken=' . $this->token .'&wsfunction=';
      }

    public function criarUsuarioMoodle($dados){

        $functionName = 'core_user_create_users';

        $user[0]['username'] = mb_strtolower($dados['username'],'UTF-8');
        $user[0]['password'] = $dados['password'];
        $user[0]['firstname'] = $dados['firstname'];
        $user[0]['lastname'] = $dados['lastname'];
        $user[0]['email'] = $dados['email'];
        $user[0]['auth'] = $dados['auth'];
        $user[0]['idnumber'] = $dados['idnumber'];
        $user[0]['timezone'] = $dados['timezone'];
        $user[0]['mailformat'] = $dados['mailformat'];
        $user[0]['city'] = $dados['city'];
        $user[0]['country'] = $dados['country'];

        $params = array('users' => $user);

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl, $params);
            
           
   }

    public function matricularCursoMoodle($request){

        $functionName = 'enrol_manual_enrol_users';

        $enrolments[0]['roleid']= $request->id_tipo_usuario_moodle;
        $enrolments[0]['userid']= $request->moodle_id;
        $enrolments[0]['courseid']= $request->curso_moodle_id;

        $params = array('enrolments' => $enrolments);

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl, $params);
            
    }

    public function definirTipoUsuarioMoodle($request){

        $functionName = 'core_role_assign_roles';

        $assignment = array( 'roleid' => intval($request->id_tipo_usuario_moodle), 'userid' => $request->moodle_id, 'contextid' => 1);
        $assignments = array( $assignment);

        $params = array('assignments' => $assignments);

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl, $params);
            
    }

    public function removerTipoUsuarioMoodle($tipoId, $moodleId){

        $functionName = 'core_role_unassign_roles';

        $unassignment = array( 'roleid' => intval($tipoId), 'userid' => $moodleId, 'contextid' => 1);
        $unassignments = array( $unassignment);

        $params = array('unassignments' => $unassignments);

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl, $params);
            
    }

    public function listarCursosMoodle(){

        $functionName = 'core_course_get_courses';
        
        $serverurl =  $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl);

    }

    public function listarCategoriasCursosMoodle(){

        $functionName = 'core_course_get_categories';

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl);

    }

    public function getCategoriaMoodle($categoriaId){

        $functionName = 'core_course_get_categories';

        $criteria[0]['key']= 'id';
        $criteria[0]['value']= $categoriaId;
        
        $params = array('criteria' => $criteria);

        $serverurl = $this->url . $functionName . '&moodlewsrestformat=json';
        require_once ('curl.php');
        $curl = new curl;
        return $curl->post($serverurl, $params);

    }
}
