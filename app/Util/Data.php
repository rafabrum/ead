<?php
namespace App\Util;

use Carbon\Carbon;
use Validator;

class Data
{

    public $data;

    public function __construct($timezone = 'America/Sao_Paulo')
    {
        $this->data = new Carbon(null, $timezone);
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Retorna a data de nascimento máxima permitida de acordo com o padrão
     * de idade permitida.
     * @return string
     */
    public static function obterDataNascimentoMaximaPermitida($formato = "Y-m-d")
    {
        return date($formato, strtotime(sprintf('-%s years', Constantes::IDADE_MINIMA_PERMITIDA)));
    }

    public static function parseDateTimeZone($data, $formato = 'Y-m-d H:i:s', $retornarString = true, $timeZone = "America/Sao_Paulo")
    {
        if ($retornarString) {
            return \Carbon\Carbon::createFromFormat($formato, $data)->timezone($timeZone)->toDateTimeString();
        }
        return \Carbon\Carbon::createFromFormat($formato, $data)->timezone($timeZone);
    }

    public static function formatDateTimeZone($data, $formato = 'd/m/Y H:i', $timeZone = "America/Sao_Paulo")
    {
        return \Carbon\Carbon::parse($data)->timezone($timeZone)->format($formato);
    }

    public static function formatarDataBrParaMySql($dataBr)
    {
        if (empty($dataBr) || !self::ehDataValida($dataBr)) {
            return null;
        }

        if (!str_contains($dataBr, '/')) {
            return $dataBr;
        }

        $dataHora = explode(' ', $dataBr);
        $data = $dataHora[0];
        $hora = array_key_exists(1, $dataHora) ? $dataHora[1] : '';
        $data = explode('/', $data);
        $data = $data[2] . '-' . $data[1] . '-' . $data[0];
        return trim($data . ' ' . $hora);
    }

    public static function formatarData($dataOriginal, $formatoFinal = 'd/m/Y')
    {
        if (empty($dataOriginal)) {
            return $dataOriginal;
        }
        try {
            $date = new \DateTime($dataOriginal);
            return $date->format($formatoFinal);
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function obterAnosDiferencaDataString($data)
    {
        if (empty($data)) {
            return null;
        }
        return Carbon::parse($data)->diffInYears();
    }

    public static function obterDiasDiferencaDataCarbon($data)
    {
        if (empty($data)) {
            return null;
        }
        return $data->diffInDays();
    }

    public static function obterDescricaoUltimaAtualizacao($data, $usarCabon = false, $timeZone = "America/Sao_Paulo")
    {
        \Carbon\Carbon::setLocale('pt_BR');
        if (empty($data)) {
            return null;
        }
        if (is_string($data)) {
            $data = Carbon::parse($data, $timeZone);
        }

        if ($usarCabon) {
            return $data->diffForHumans();
        }
        $diasData = $data->diffInDays();
        $resposta = null;

        if ($diasData > 1) {
            $resposta = $diasData . " dias atrás";
        } elseif ($diasData == 1) {
            $resposta = "ontem";
        } else {
            $resposta = "hoje";
        }
        return $resposta;
    }

    /**
     * Método verifica se a data informada é uma data válida.
     * Exemplo formatos: '2016-05-02T15:05:38+0000', '2016-05-01', '01/05/2016'
     * @param string $data
     * @return boolean
     */
    public static function ehDataValida($data)
    {
        if (empty($data)) {
            return false;
        }

        if (strpos($data, "/") !== false) {
            $dataBr = explode("/", $data);

            if (empty($dataBr[0]) || empty($dataBr[1]) || empty($dataBr[2])) {
                return false;
            }

            $ehDataBrValida = checkdate((int) $dataBr[1], (int) $dataBr[0], (int) $dataBr[2]);
            return $ehDataBrValida;
        }

        $dados['data'] = $data;
        $validator = Validator::make($dados, [
                'data' => 'date',
        ]);

        return !$validator->fails();
    }

    public static function ehPeriodoDiasValido($attribute, $value, $parameters)
    {
        $dataInicio = Carbon::parse(Data::formatarDataBrParaMySql($value));
        $dataFim = Carbon::parse(Data::formatarDataBrParaMySql($parameters[0]));
        $limiteDiasPeriodo = (int) $parameters[1];
//        dd($attribute, $value, $parameters, $dataInicio, $dataFim, $dataFim->diffInDays($dataInicio), $limiteDiasPeriodo);
        return $dataFim->diffInDays($dataInicio) <= $limiteDiasPeriodo;
    }
}
