<div class="modal inmodal fade cadastrar-landing-page" id="modalSalvarLandingPage{{ $landingPage ? $landingPage->id : '' }}" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 900px; min-height: 900px;">
        <div class="modal-content">
            <div class="modal-header" style="margin-bottom: 20px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Nova Landing-page</h4>
            </div>
            <form class="wizard-big course-finder-form form-landing-page" action="{{ url('marketing/landing-page') }}" method="POST">
                {{ csrf_field() }}

                <h1>Identificação/Topo da página</h1>
                <fieldset>

                    <div class="col-lg-8" >

                        <label class="col-lg-2 control-label">Nome</label>

                        <div class="col-lg-10">
                            <input type="text" name="nome" placeholder="Nome da divulgação" value="{{ old('nome') ? old('nome') : ($landingPage ? $landingPage->nome : '') }}" class="form-control">
                            <span class="error {{ $errors->has('nome') ? '' : ' hide' }}" for="nome">{{ $errors->has('nome') ? $errors->first('nome') : '' }}</span>
                        </div><br><br><br>

                    </div>
                    
                    <div class="col-lg-8">

                        <label class="col-sm-2 control-label">Categoria</label>

                        <div class="col-sm-10">
                            <select id="categoria" name="categoria" class="form-control select-categoria m-b" data-url-categoria="{{ route('getCategoriasJson') }}" data-url-unidades="{{ route('getUnidadesJson', ['?'])}}" required>
                                <option value="">Selecione uma categoria...</option>
                                @foreach($categorias as $categoria)
                                <option value="{{ $categoria->id_categoria_curso }}">{{ $categoria->titulo_categoria }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-lg-8">


                        <label class="col-sm-2 control-label">Unidade</label>

                        <div class="col-sm-10">
                            <select class="form-control m-b" name="unidade" id="unidades" data-inicializar="Selecione uma unidade..." data-nao-encontrado="Nenhuma unidade encontrada..." data-url-cursos="{{ route('getCursosJson', ['?', '?'])}}" required>
                                <option value="">Selecione uma unidade...</option>
                            </select>
                        </div>
                        
                    </div>  
                    <div class="col-lg-8">


                        <label class="col-sm-2 control-label">Curso</label>

                        <div class="col-sm-10">
                            <select class="form-control m-b" name="id_curso" id="cursos" data-inicializar="Selecione um curso..." data-nao-encontrado="Nenhuma curso encontrado..." required>
                                <option value="">Selecione um curso...</option>
                            </select>
                        </div>

                    </div>

                </fieldset>

                <h1>O Curso</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Apresentação do Curso</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="descricao" value="{{ old('descricao') ? old('descricao') : ($landingPage ? $landingPage->descricao : '') }}">
                            </textarea>

                        </div>
                    </div>
                </fieldset>

                <h1>Objetivo</h1>
                <fieldset>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Objetivo</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="objetivo" value="{{ old('objetivo') ? old('objetivo') : ($landingPage ? $landingPage->objetivo : '') }}">
                            </textarea>

                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Publico Alvo</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="publico_alvo" value="{{ old('publico_alvo') ? old('publico_alvo') : ($landingPage ? $landingPage->publico_alvo : '') }}">
                            </textarea>

                        </div>
                    </div>

                </fieldset>

                <h1>Corpo docente</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Docente</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="docente" value="{{ old('docente') ? old('docente') : ($landingPage ? $landingPage->docente : '') }}">
                            </textarea>

                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Coordenação</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="coordenacao" value="{{ old('coordenacao') ? old('coordenacao') : ($landingPage ? $landingPage->coordenacao : '') }}">
                            </textarea>

                        </div>
                    </div>
                </fieldset>

                <h1>Disciplinas</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Objetivo</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="disciplina" value="{{ old('disciplina') ? old('disciplina') : ($landingPage ? $landingPage->disciplina : '') }}">
                            </textarea>

                        </div>
                    </div>
                </fieldset>

                <h1>Informações</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Informações complementares</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="informacoes_complementares" value="{{ old('informacoes_complementares') ? old('informacoes_complementares') : ($landingPage ? $landingPage->informacoes_complementares : '') }}">
                            </textarea>

                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Diferenciais</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="diferenciais" value="{{ old('diferenciais') ? old('diferenciais') : ($landingPage ? $landingPage->diferenciais : '') }}">
                            </textarea>

                        </div>
                    </div>

                </fieldset>

                <h1>Proximas turmas</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>proximas turmas</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="proximas_turmas" value="{{ old('proximas_turmas') ? old('proximas_turmas') : ($landingPage ? $landingPage->proximas_turmas : '') }}">
                            </textarea>

                        </div>
                    </div>
                </fieldset>

                <h1>Certificação / Proxima data</h1>
                <fieldset>
                    <h2></h2>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Certificação</h5>

                        </div>
                        <div class="ibox-content no-padding">
                            <textarea  class="summernote{{ $landingPage ? $landingPage->id : '' }}" name="certificacao" value="{{ old('certificacao') ? old('certificacao') : ($landingPage ? $landingPage->certificacao : '') }}">
                            </textarea>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input type="date" name="data_proxima_turma" class="form-control" value="{{ old('data_proxima_turma') ? old('data_proxima_turma') : ($landingPage ? $landingPage->data_proxima_turma : '') }}"></input>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
</div>
