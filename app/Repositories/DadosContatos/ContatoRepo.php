<?php
namespace App\Repositories\DadosContatos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Contato;

class ContatoRepo implements IContatoRepo
{

	public function criarContato($request){
		
		return Contato::create([
                'tel' =>  $request->tel,
                'cel'  =>  $request->cel, 
                'email_alternativo'  =>  $request->email_alternativo, 
                'user_id' => $request->user_id,
               ]);
	}

	public function alterarContato($request){

		return Contato::where('user_id', $request->user_id)
              ->update([
                'tel' =>  $request->tel,
                'cel'  =>  $request->cel, 
                'email_alternativo'  =>  $request->email_alternativo, 
               ]);
	}
}
