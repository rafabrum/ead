<?php
namespace App\Repositories\DadosContatos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Endereco;

class EnderecoRepo implements IEnderecoRepo
{

	public function criarEndereco($request){
		
		return Endereco::create([
                'end_rua' =>  $request->end_rua,
                'end_numero'  =>  $request->end_numero, 
                'end_complemento'  =>  $request->end_complemento, 
                'end_bairro' => $request->end_bairro,
                'end_cidade' => $request->end_cidade,
                'end_cep' => $request->end_cep,
                'end_estado' => $request->end_estado,
                'user_id' => $request->user_id,
               ]);
	}

    public function alterarEndereco($request){

        return Endereco::where('user_id', $request->user_id)
              ->update([
                'end_rua' =>  $request->end_rua,
                'end_numero'  =>  $request->end_numero, 
                'end_complemento'  =>  $request->end_complemento, 
                'end_bairro' => $request->end_bairro,
                'end_cidade' => $request->end_cidade,
                'end_cep' => $request->end_cep,
                'end_estado' => $request->end_estado,
               ]);

    }
}
