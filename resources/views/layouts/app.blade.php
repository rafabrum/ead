<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head-pagina')
</head>

<body>
    @include('partials.loading-page')

    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')
        </div>
    </div>

    <script src="{{ elixir('assets/template/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ elixir('assets/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ elixir('assets/template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ elixir('assets/template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Peity -->
    <script src="{{ elixir('assets/template/js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ elixir('assets/template/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ elixir('assets/template/js/inspinia.js') }}"></script>
    <script src="{{ elixir('assets/template/js/plugins/pace/pace.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ elixir('assets/template/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- GITTER -->
    <script src="{{ elixir('assets/template/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ elixir('assets/template/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ elixir('assets/template/js/demo/sparkline-demo.js') }}"></script>

    <!-- ChartJS-->
    <script src="{{ elixir('assets/template/js/plugins/chartJs/Chart.min.js') }}"></script>

    <!-- Toastr -->
    <script src="{{ elixir('assets/template/js/plugins/toastr/toastr.min.js') }}"></script>


    @yield('scripts-plugins')
    @yield('scripts')

    @include('partials.inicializar-modulos')

</body>
</html>
