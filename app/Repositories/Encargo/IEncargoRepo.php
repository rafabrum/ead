<?php

namespace App\Repositories\Encargo;

interface IEncargoRepo
{
	public function criarEncargo($request);
	public function criarTipoEncargo($request);
}
