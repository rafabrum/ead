<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoriaMenu;
use App\Models\Menu;
use App\Repositories\Menu\IMenuRepo;
use DB;

class MenuController extends Controller
{
    public function viewListaMenu(Request $equest){

        $menus = Menu::all();

        return view('menu.lista')
                    ->with('menus', $menus);
    }

    public function alterarMenu(Request $request, IMenuRepo $menu){

        $aterarMenu = $menu->alterarMenu($request);

    	return $this->tratarRespostaComAlertaNotificacao( "OK!", "Menu salvo com sucesso!", route('listaMenu'));
    }

    public function novoMenu(Request $request, IMenuRepo $menu){

        $novoMenu = $menu->criarMenu($request);

        return $this->tratarRespostaComAlertaNotificacao( "OK!", "Menu cadastrado com sucesso!", route('listaMenu'));

    }

    public function alterarCategoria(Request $request, IMenuRepo $menu){

        //Verificar se já existe outra categoria com a ordem requisitada
        if(CategoriaMenu::where('ordem', $request->ordem)->count() > 0){
            //inverte a ordem existente com a ordem atual da categoria requisitada
            $ordemAtual = CategoriaMenu::where('id', $request->categoria_id)->first();
                $invert = CategoriaMenu::where('ordem', $request->ordem)->update(['ordem' => $ordemAtual->ordem]);
        }

        $alterarCategoria = $menu->alterarCategoria($request);

       return $this->tratarRespostaComAlertaNotificacao( "OK!", "Categoria salva com sucesso!", route('listaMenu'));
    }

    public function novaCategoria(Request $request, IMenuRepo $menu){

        $verificarOrdem = CategoriaMenu::where('ordem', $request->ordem);

        if(CategoriaMenu::where('ordem', $request->ordem)->count() > 0){
            $novaOrdem = CategoriaMenu::where('ordem', '>=', $request->ordem)->get();
            foreach ($novaOrdem as $ordem){
                $invert = CategoriaMenu::where('id', $ordem->id)->update(['ordem' => $ordem->ordem + 1]); 
            }
        }

        $novoCategoria = $menu->criarCategoria($request);

        return $this->tratarRespostaComAlertaNotificacao( "OK!", "categoria cadastrada com sucesso!", route('listaMenu'));
    }
}
