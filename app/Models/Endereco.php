<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
	protected $table = 'endereco';
    protected $primaryKey = 'id';

    protected $fillable = ["end_rua", "end_numero", "end_complemento", "end_bairro",
							"end_cidade", "end_cep", "end_estado", "user_id"];

	public function user()
    {
         return $this->belongsTo('App\User', "id", "user_id");
    }
}
