<?php

use Illuminate\Database\Seeder;

class CategoriaMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria_menu')->insert([
            'id' => 1,
            'nome' => 'Aluno',
            'ordem' => 1,
            'icone' => 'fa fa-graduation-cap'
        ]);

        DB::table('categoria_menu')->insert([
            'id' => 2,
            'nome' => 'Administração',
            'ordem' => 4,
            'icone' => 'fa fa-address-book-o'
        ]);

        DB::table('categoria_menu')->insert([
            'id' => 3,
            'nome' => 'Relatórios',
            'ordem' => 3,
            'icone' => 'fa fa-bar-chart'
        ]);

        DB::table('categoria_menu')->insert([
            'id' => 4,
            'nome' => 'Curso',
            'ordem' => 2,
            'icone' => 'fa fa-pencil'
        ]);

        DB::table('categoria_menu')->insert([
            'id' => 5,
            'nome' => 'Desenvolvimento',
            'ordem' => 5,
            'icone' => 'fa fa-cogs'
        ]);
    }
}
           