<?php
namespace App\Repositories\Encargo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Encargo;
use App\Models\TipoEncargo;

class EncargoRepo implements IEncargoRepo
{

	public function criarEncargo($request){
		
		return Encargo::create([
                'vencimento' =>  $request->vencimento,
                'valor'  =>  $request->valor, 
                'tipo_id'  =>  $request->tipo_id, 
                'aluno_id' => $request->aluno_id,
                'curso_id' => $request->curso_moodle_id,
               ]);
	}
    public function criarTipoEncargo($request){
        
        return TipoEncargo::create([
                'nome_encargo' =>  $request->formacao,
                'descricao'  =>  $request->instituicao, 
               ]);
    }
}
