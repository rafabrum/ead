@extends('layouts.app')
@section('styles')
<link href="{{ elixir('assets/template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-sm-4">
    <h2>Lista de alunos</h2>
  </div>
  <div class="col-sm-8 m-t-lg">
    <div class="col-sm-5 pull-right">
      <form method="GET" class="m-b-md form-pesquisa" action="{{ Request::url() }}">
        <div class="input-group">
          <select type="text" name="categoria" class="input-sm form-control">
            @if(!Session::has('filtro'))
            <option value="">Selecione uma categoria</option>
            @endif
            @foreach($categorias as $categoria)
            @if(Session::get('filtro') == $categoria->id)
            <option value="{{$categoria->id}}">{{$categoria->name}}</option>
            @endif
            @endforeach
            @foreach($categorias as $categoria)
            @if(Session::get('filtro') != $categoria->id)
            <option value="{{$categoria->id}}">{{$categoria->name}}</option>
            @endif
            @endforeach
          </select> 
          <span class="input-group-btn">
            <button type="submit" class="btn btn-sm btn-primary">Filtrar</button>
          </span>

        </div>
      </form>
    </div>
  </div>
</div>
<div id="recuperarModalDadosPessoais" data-modal="modalAlterarDadosPessoais{{Session::get('modalDadosPessoais')}}"></div>
<div id="recuperarModalEnderecoContato" data-modal="modalAlterarEnderecoContato{{Session::get('modalEnderecoContato')}}"></div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">

        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover datatables-lista">
              <thead>
                <tr>
                  <th>Alunos</th>
                  <th class="col-lg-2">Documentação</th>
                  <th class="col-lg-2">Acadêmico</th>
                  <th class="col-lg-2">Financeiro</th>
                </tr>
              </thead>
              <tbody>
                @foreach($alunos as $aluno)
                <tr class="gradeX">
                  <td>{{$aluno->aluno->user->name}} {{$aluno->aluno->user->sobrenome}}</td>
                  <td>
                    <div class="btn-group pull-right">
                      <button data-toggle="dropdown" title="Ficha financeira" class="btn btn-default dropdown-toggle"><i class="fa fa-file-pdf-o"></i></button>
                    </div>
                  </td>
                  <td>
                    <div class="btn-group pull-right">
                      <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="fa fa-mortar-board"></i>&nbsp;<span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a data-toggle="modal" data-target="#modalAlterarDadosPessoais{{$aluno->aluno->id}}" href="" title="Dados Pessoais"><i></i>Dados Pessoais</a></li>
                        <li><a data-toggle="modal" data-target="#modalAlterarEnderecoContato{{$aluno->aluno->id}}" href="" title="Endereço / Contato"><i></i>Endereço / Contato</a></li>
                        <li><a href="" title="Nova Senha"><i></i>Alterar Senha</a></li>
                      </ul>
                    </div>
                  </td>
                  <td>
                    <div class="btn-group pull-right">
                      <button data-toggle="dropdown" title="Ficha financeira" class="btn btn-default dropdown-toggle"><i class="fa fa-barcode"></i></button>
                    </div>
                  </td>
                </tr>
                @include('partials.modals.aluno.alterarDadosPessoais', ['aluno' => $aluno->aluno])
                @include('partials.modals.aluno.alterarEnderecoContato', ['aluno' => $aluno->aluno])
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>Alunos</th>
                  <th class="col-lg-2">Documentação</th>
                  <th class="col-lg-2">Acadêmico</th>
                  <th class="col-lg-2">Financeiro</th>
                 </tr>
               </tfoot>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 @endsection
 @section('scripts')
 <script src="{{ elixir('assets/template/js/plugins/dataTables/datatables.min.js') }}"></script>
 <script src="{{ elixir('assets/js/datatables-lista.js') }}"></script>
 @if(Session::has('errors') && Session::has('modalDadosPessoais'))
<script type="text/javascript">
    $(document).ready(function () {
        var modal = $('#recuperarModalDadosPessoais').data().modal;       
        $('#'+ modal).modal('show');
    });
</script>
@elseif(Session::has('errors') && Session::has('modalEnderecoContato'))
<script type="text/javascript">
    $(document).ready(function () {
        var modalSenha = $('#recuperarModalEnderecoContato').data().modal;       
        $('#'+ modalSenha).modal('show');
    });
</script>
@endif
@endsection