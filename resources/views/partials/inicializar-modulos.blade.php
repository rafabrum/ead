    @if(Session::has('exibirAlertaNotificacao'))
    <script type="text/javascript">
    	var exibirAlertaNotificacao = {!! json_encode(Session::get('exibirAlertaNotificacao')) !!};
        $(document).ready(function () {
           toastr.success(exibirAlertaNotificacao.mensagem, exibirAlertaNotificacao.titulo)
        });
    </script>
    @endif