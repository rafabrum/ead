<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
	protected $table = 'contatos';
    protected $primaryKey = 'id';

    protected $fillable = ["tel", "cel", "email_alternativo", "user_id"];

	public function user()
    {
         return $this->belongsTo('App\User', "id", "user_id");
    }
}
