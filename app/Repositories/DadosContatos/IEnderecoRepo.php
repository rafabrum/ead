<?php

namespace App\Repositories\DadosContatos;

interface IEnderecoRepo
{
	public function criarEndereco($request);

	public function alterarEndereco($request);
}
