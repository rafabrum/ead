<?php

use Illuminate\Database\Seeder;

class ColaboradoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $data = "2018-08-21 03:00:00";

    public function run()
    {
    	DB::table('colaboradores')->insert([
    		'id' => 1,
    		'unidade' => '-',
    		'ramal' => 0,
    		'status' => 'ativo',
    		'formacao' => '-',
    		'created_at' => $this->data,
    		'updated_at' => $this->data,
    		'user_id' => 1,
    		'moodle_id' => 0,
            'usuario_moodle' => 'administrador',
            'senha_moodle' => 'eG1mb2JrNTMzMg==',
            'tipo_usuario_moodle' => 'admin',
            'id_tipo_usuario_moodle' => '0'
    	]);
    }
}
