<?php
namespace App\Util;

use App\Models\CategoriaMenu;

class Constantes
{
	const ID_ALUNO_MOODLE = 5;
	const TIPO_MATRICULA = 2;
	const TIPO_PARCELA = 1;
}
