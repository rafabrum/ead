<div class="modal inmodal fade alterar-menu" id="modalNovoMenu{{$categoria->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Novo Menu</h4>
            </div>
            <form role="form" class="form-menu" method="POST" action="{{ route('postNovoMenu') }}">
				<div class="modal-body">
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								{{ csrf_field() }}
								<div class="col-md-6">
									<div class="form-group">
										<label>Titulo</label>
										<input type="text" name="titulo" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Alias</label>
										<input type="text" name="alias" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Url</label><span><i style="color:red">&nbsp;* Lembre de criar uma Rota</i></span>
										<input type="text" name="url" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Categoria</label>
										<select name="categoria_id" id="categoria_id" class="form-control">
											<option value="{{$categoria->id}}">{{$categoria->nome}}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" id="btnSalvarCampanha">Salvar</button>
				</div>
			</form>
        </div>
    </div>
</div>
