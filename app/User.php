<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'sobrenome',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function colaborador()
    {
        return $this->hasOne('App\Models\Colaborador', "user_id", "id");
    }

    public function aluno()
    {
        return $this->hasOne('App\Models\Aluno', "user_id", "id");
    }

    public function dadosPessoais()
    {
        return $this->hasOne('App\Models\DadosPessoais', "user_id", "id");
    }

    public function endereco()
    {
        return $this->hasOne('App\Models\Endereco', "user_id", "id");
    }

    public function contato()
    {
        return $this->hasOne('App\Models\Contato', "user_id", "id");
    }
}
