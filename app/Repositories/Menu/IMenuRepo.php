<?php

namespace App\Repositories\Menu;

interface IMenuRepo
{
    public function criarMenu($request);
    public function criarCategoria($request);
    public function alterarMenu($request);
    public function alterarCategoria($request);
}
