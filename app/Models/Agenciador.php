<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agenciador extends Model
{
	protected $table = 'agenciadores';
	protected $primaryKey = 'id';

	protected $fillable = ["local", "status", "valor_fixo", "capacitacao", "percentual_comissao", "taxa_matricula", "							  percentual_matricula", "usuario_moodle", "senha_moodle", "user_id"];

	public function user()
	{
		return $this->belongsTo('App\User', "id", "user_id");
	}
}