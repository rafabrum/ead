<div class="modal inmodal fade alterar-menu" id="modalValidacaoAluno" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
				<h4 class="modal-title">Confira os seguintes campos</h4>
			</div>
			<div class="lista-validacao">
				<div>
					@if ($errors->has('name'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*CAMPO NOME: {{ $errors->first('name') }}</h3>
					</span>
					@endif
					@if ($errors->has('sobrenome'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*CAMPO SOBRENOME: {{ $errors->first('sobrenome') }}</h3>
					</span>
					@endif
					@if ($errors->has('cpf'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*CAMPO CPF: {{ $errors->first('cpf') }}</h3>
					</span>
					@endif
					@if ($errors->has('email'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*CAMPO EMAIL: {{ $errors->first('email') }}</h3>
					</span>
					@endif
					@if ($errors->has('curso_moodle_id'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*CAMPO CURSO: {{ $errors->first('curso_moodle_id') }}</h3>
					</span>
					@endif
					@if ($errors->has('colaborador_id'))
					<span class="help-block" style="text-align: center;">
						<h3 style="color:red">*TIPO DE VENDA: {{ $errors->first('colaborador_id') }}</h3>
					</span>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
