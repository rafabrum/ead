<?php

namespace App\Repositories\Agenciador;

interface IAgenciadorRepo
{
	public function criarAgenciador($request);
}
