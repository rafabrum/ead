<?php
namespace App\Repositories\DadosContatos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\DadosPessoais;

class DadosPessoaisRepo implements IDadosPessoaisRepo
{

	public function criarDadosPessoais($request){
		
		return DadosPessoais::create([
                'cpf' =>  $request->cpf,
                'rg'  =>  $request->rg, 
                'emissor'  =>  $request->emissor, 
                'nascimento' => $request->nascimento,
                'nacionalidade' => $request->nacionalidade,
                'naturalidade' => $request->naturalidade,
                'sexo' => $request->sexo,
                'pai' => $request->pai,
                'mae' => $request->mae,
                'user_id' => $request->user_id,
               ]);
	}

    public function alterarDadosPessoais($request){

        return DadosPessoais::where('user_id', $request->user_id)
              ->update([
                'cpf' =>  $request->cpf,
                'rg'  =>  $request->rg, 
                'emissor'  =>  $request->emissor, 
                'nascimento' => $request->nascimento,
                'nacionalidade' => $request->nacionalidade,
                'naturalidade' => $request->naturalidade,
                'sexo' => $request->sexo,
                'pai' => $request->pai,
                'mae' => $request->mae,
               ]);

    }
}
