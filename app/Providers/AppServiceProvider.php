<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); 
    }

 /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Menu\IMenuRepo', 'App\Repositories\Menu\MenuRepo'
        );
        $this->app->bind(
            'App\Repositories\Moodle\IMoodleRepo', 'App\Repositories\Moodle\MoodleRepo'
        );
        $this->app->bind(
            'App\Repositories\Colaborador\IColaboradorRepo', 'App\Repositories\Colaborador\ColaboradorRepo'
        );
        $this->app->bind(
            'App\Repositories\Usuario\IUsuarioRepo', 'App\Repositories\Usuario\UsuarioRepo'
        );
        $this->app->bind(
            'App\Repositories\Aluno\IAlunoRepo', 'App\Repositories\aluno\AlunoRepo'
        );
        $this->app->bind(
            'App\Repositories\Aluno\IAlunoCursoRepo', 'App\Repositories\aluno\AlunoCursoRepo'
        );
        $this->app->bind(
            'App\Repositories\DadosContatos\IContatoRepo', 'App\Repositories\DadosContatos\ContatoRepo'
        );
        $this->app->bind(
            'App\Repositories\DadosContatos\IDadosPessoaisRepo', 'App\Repositories\DadosContatos\DadosPessoaisRepo'
        );
        $this->app->bind(
            'App\Repositories\DadosContatos\IEnderecoRepo', 'App\Repositories\DadosContatos\EnderecoRepo'
        );
        $this->app->bind(
            'App\Repositories\Agenciador\IAgenciadorRepo', 'App\Repositories\Agenciador\AgenciadorRepo'
        );
        $this->app->bind(
            'App\Repositories\Encargo\IEncargoRepo', 'App\Repositories\Encargo\EncargoRepo'
        );
    }
}
