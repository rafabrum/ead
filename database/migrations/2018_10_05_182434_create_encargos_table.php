<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encargos', function (Blueprint $table) {
           $table->increments('id');
           $table->timestamps();
           $table->date('vencimento');
           $table->decimal('valor', 8,2);
           $table->unsignedinteger('tipo_id');
           $table->unsignedinteger('aluno_id');

           $table->integer('curso_id')->nullable();

           $table->foreign('tipo_id')->references('id')->on('tipos_encargos');
           $table->foreign('aluno_id')->references('id')->on('alunos');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encargos');
    }
}
