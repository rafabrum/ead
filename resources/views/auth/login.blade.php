@extends('layouts.app-simples')
@section('styles')
<link href="{{ elixir('assets/css/login.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
    <div class="middle-box text-left loginscreen animated fadeInDown">
        <div class="box-login">
            <div class="row">
                <img id="icon-loading" style="width: 250px; margin-bottom: 15px;"   src="{{ URL::asset('assets/imagens/logo.png') }}" alt="">
            </div>
            
            <p>Entre com seu e-mail e senha</p>
            <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" placeholder="Email" name="email" required="" value="{{ old('email') }}">
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Senha" name="password" required="" value="{{ old('password') }}">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="#"><small>Esqueceu a senha?</small></a>
            </form>
        </div>
    </div>
</div>
@endsection
