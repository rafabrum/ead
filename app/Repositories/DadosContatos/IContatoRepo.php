<?php

namespace App\Repositories\DadosContatos;

interface IContatoRepo
{
	public function criarContato($request);

	public function alterarContato($request);
}
