<?php

namespace App\Repositories\Colaborador;

interface IColaboradorRepo
{
	public function alterarColaborador($request);
	public function criarColaborador($request);
}
