<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();
            $table->string('formacao', 60)->nullable();
            $table->string('instituicao', 60)->nullable();
            $table->string('ano_formacao')->nullable();
            $table->string('status', 30);
            $table->integer('moodle_id')->nullable();
            $table->string('usuario_moodle')->nullable();
            $table->string('senha_moodle')->nullable();

            $table->unsignedinteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('alunos');
    }
}
