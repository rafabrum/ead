<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DadosPessoais extends Model
{
    protected $table = 'dados_pessoais';
    protected $primaryKey = 'id';
    
	protected $fillable = ["cpf", "rg", "emissor", "nascimento",
							"nacionalidade", "naturalidade", "sexo", "pai", "mae", "user_id"];

	public function user()
    {
         return $this->belongsTo('App\User', "id", "user_id");
    }
}
