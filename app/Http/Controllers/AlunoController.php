<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Moodle\IMoodleRepo;
use App\Repositories\DadosContatos\IDadosPessoaisRepo;
use App\Repositories\DadosContatos\IContatoRepo;
use App\Repositories\DadosContatos\IEnderecoRepo;
use App\Repositories\Usuario\IUsuarioRepo;
use App\Models\Colaborador;
use App\Models\Aluno;
use App\Models\AlunoCurso;
use Session;
use DB;

class AlunoController extends Controller
{

    public function viewNovoAluno(IMoodleRepo $moodle){

        $colaboradores = Colaborador::all();
        $cursos = json_decode($moodle->listarCursosMoodle());
        return view('aluno.novo')
        ->with('colaboradores', $colaboradores)
        ->with('cursos', $cursos);

    }

    public function viewAlterarAluno($alunoId){

        $aluno = Aluno::find($alunoId);

        return view('aluno.alterar')
        ->with('aluno', $aluno);
    }

    public function postAlterarDadosPessoaisAluno(Request $request, IDadosPessoaisRepo $dadosPessoais, IUsuarioRepo $usuario){

        Session::flash('modalDadosPessoais', $request->aluno_id);

        $this->validarDadosPessoais($request);

        $usuario->alterarUsuario($request);
        $dadosPessoais->alterarDadosPessoais($request);

        $url = url()->previous();

        return $this->tratarRespostaComAlertaNotificacao( "OK!", "Dados Pessoais salvo com sucesso!", $url);
    }

    public function postAlterarEnderecoContatoAluno(Request $request, IEnderecoRepo $endereco, IContatoRepo $contato){

        Session::flash('modalEnderecoContato', $request->aluno_id);

        $this->validarEnderecoContato($request);

        $contato->alterarContato($request);
        $endereco->alterarEndereco($request);

        $url = url()->previous();

        return $this->tratarRespostaComAlertaNotificacao( "OK!", "Endereço / Contato salvo com sucesso!", $url);
    }

    public function viewListaAluno(Request $request, IMoodleRepo $moodle){

        if($request->categoria){
            Session::flash('filtro', $request->categoria);
        }

        $alunos = AlunoCurso::where('curso_moodle_categoria_id', $request->categoria)->get();

        $categorias = json_decode($moodle->listarCategoriasCursosMoodle());

        return view('aluno.lista')
        ->with('categorias', $categorias)
        ->with('alunos', $alunos);

    }

    protected function validarDadosPessoais($request)
    {

        $this->validate($request, [
            'name' => 'required|string|min:3|max:40',
            'sobrenome' => 'required|string|min:3|max:40',
            'cpf' => 'required|min:9|numeric|unique:dados_pessoais,cpf,'.$request->user_id,
        ], [
            'required' => 'Campo obrigatório',
            'min' => 'Mínimo de 3 caracteres',
            'max' => 'Máximo de 40 caracteres',
            'cpf.min' => 'O cpf deve conter 11 caracteres',
            'cpf.unique' => 'Já existe no sistema um usuário com esse cpf'
        ]);
    }

    protected function validarEnderecoContato($request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->user_id,
        ], [
            'required' => 'Campo obrigatório',
            'email.unique' => 'Já existe um usuário com esse email',
            'email.email' => 'O email precisa ser válido',
        ]);
    }

}
