@extends('layouts.app')
@section('scripts')
<script type="text/javascript">
	window.onload = function (){
		$("#formMoodle").submit();
	}
</script>
@endsection

@section('content')


<div class="panel-group">
	<form method="POST" id="formMoodle" action="http://devmoodle.ineead.com.br/login/index.php">
		<input type="hidden" name="username" value="{{$usuario}}">
		<input type="hidden" name="password" value="{{$senha}}">
	</form>
</div>
<br><br><br>
<div class="col-md-12">   		
	<img id="icon-loading" style="width: 250px;" class="col-md-4 col-md-offset-4"  src="{{ URL::asset('assets/imagens/loading.gif') }}" alt="">
</div>


@include('partials.modals.menu.cadastrar-categoria')
@endsection
