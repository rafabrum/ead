<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaboradores', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();
            $table->string('unidade', 50)->nullable();
            $table->integer('ramal')->nullable();
            $table->string('status', 30)->nullable();
            $table->string('formacao', 60)->nullable();
            $table->integer('moodle_id')->nullable();
            $table->string('usuario_moodle')->nullable();
            $table->string('senha_moodle')->nullable();
            $table->string('tipo_usuario_moodle')->nullable();
            $table->string('id_tipo_usuario_moodle')->nullable();

            $table->unsignedinteger('user_id');

             $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colaboradores');
    }
}
